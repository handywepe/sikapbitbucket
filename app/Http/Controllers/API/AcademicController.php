<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Academic;
use Illuminate\Http\Request;

class AcademicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function save(Request $request)
    {
        foreach ($request->academic as $academic) {
            if ($academic['id'] != null) {
                $this->update($academic, $academic['id']);
            }else{
                $this->store($academic);
            }
        }
        return ['message' => 'Success'];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($data)
    {
        Academic::create([
            'id_user' => $data['id_user'],
            'tingkat' => $data['tingkat'],
            'jurusan' => $data['jurusan'],
            'nama' => $data['nama'],
            'tahun' => $data['tahun'],
        ]);

        return ['message' => 'Success'];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Academic  $academic
     * @return \Illuminate\Http\Response
     */
    public function update($data)
    {
        $academic = Academic::findOrFail($data['id']);
        $academic->update([
            'tingkat' => $data['tingkat'],
            'jurusan' => $data['jurusan'],
            'nama' => $data['nama'],
            'tahun' => $data['tahun'],
        ]);
        return ['message' => "success"];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Academic  $academic
     * @return \Illuminate\Http\Response
     */
    public function show(Academic $academic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Academic  $academic
     * @return \Illuminate\Http\Response
     */
    public function edit(Academic $academic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Academic  $academic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Academic $academic)
    {
        $academic->delete();
        return [ 'Message' => 'Delete Success'];
    }
}
