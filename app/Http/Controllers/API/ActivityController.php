<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Activity;
use App\Models\Skp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = date("Y-m-d");
        $user = auth('sanctum')->user()->id;
        $activity = Activity::where('id_user', '=', $user)
        ->where('tanggal', '=', $today)
        // ->select('id', 'id_placement', 'jam', 'aktivitas', 'mr', 'nama_pasien', 'status', 'tanggal')
        ->orderBy('jam', 'DESC')
        ->with([
            'skp:id,id_tupoksi',
            'skp.tupoksi:id,butir_kegiatan'
        ])
        ->get();
        return $activity;
    }

    public function latestActivity(Request $request)
    {
        $activity = Activity::where('id_user', '=', $request->id_user)
        ->orderBy('tanggal', 'DESC')
        ->with([
            'skp:id,id_tupoksi',
            'skp.tupoksi:id,butir_kegiatan'
        ])
        ->limit(5)
        ->get();
        return $activity;
    }

    public function filter($var){
        return($var & 1);
    }

    public function byDate(Request $request)
    {
        // $getAccess = Arr::where($access, function ($value, $key) use ($id) { return $value == $id ;});

        $activity = Activity::where('tanggal', '=', $request->date)
        ->where('id_user', '=', $request->id_user)
        ->with([
            'skp:id,id_tupoksi',
            'skp.tupoksi:id,butir_kegiatan'
        ])
        // ->select('id', 'id_placement', 'jam', 'aktivitas', 'mr', 'nama_pasien', 'status', 'tanggal')
        ->orderBy('jam', 'DESC')
        ->get();
        return $activity;
    }

    public function getEvent(Request $request)
    {
        //
        $id_user = $request->id_user;
        $tahun = date('Y', strtotime($request->date));
        $bulan = date('m', strtotime($request->date));
        $event = Activity::where('id_user', '=', $id_user)->where('status', '=', 'Menunggu')
        ->whereMonth('tanggal', '=', $bulan)->whereYear('tanggal', '=', $tahun)->groupBy('tanggal')
        ->select('tanggal')->pluck('tanggal');

        return $event;
    }

    public function countAssesmentDB(){
        $user = app('App\Http\Controllers\API\UserController')->userAssesment();
        $count = [];
        foreach($user as $u){
            $id_user = $u->id_user;
            $sum = count(Activity::where('id_user', '=', $u->id_user)->where('status', '=', 'Menunggu')->select('id_user')->get());
            // $push = [ 'id_user' => $id_user, 'total' => $sum ];
            if ($sum != 0) {
                array_push($count, $sum);
            }

        }
        return array_sum($count);
    }

    public function statisticDB(Request $request)
    {
        $user = Auth('sanctum')->user();
        $tahun = $request->year;
        $accept = [];
        $reject = [];

        for ($i=1; $i <= 12; $i++) {
            $push = count(Activity::where('id_user', '=', $user->id)
                    ->whereYear('tanggal', '=', $tahun)
                    ->whereMonth('tanggal', '=', $i)
                    ->where('status', '=', 'Disetujui')
                    ->get());
            array_push($accept, $push);
        }
        for ($u=1; $u <= 12; $u++) {
            $push2 = count(Activity::where('id_user', '=', $user->id)
                    ->whereYear('tanggal', '=', $tahun)
                    ->whereMonth('tanggal', '=', $u)
                    ->where('status', '=', 'Ditolak')
                    ->get());
            array_push($reject, -$push2);
        }

        $wait = count(Activity::where('id_user', '=', $user->id)
                ->whereYear('tanggal', '=', $tahun)
                ->where('status', '=', 'Menunggu')
                ->get());

        return [ 'accept' => $accept, 'reject' => $reject, 'wait' => $wait];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->loop as $key=>$activity) {
            if ($activity['base64']) {
                $file = $activity['base64'];
                $name = time().$key.'.'.explode('/', explode(':', substr($file, 0, strpos($file, ';')))[1])[1];

                $path = public_path('kegiatan/'.date("dmY").'/');
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                \Image::make($file)->resize(2000, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$name);
                $lampiran = '/kegiatan/'.date("dmY").'/'.$name;
            }else{
                $lampiran = null;
            }
            Activity::create([
                'id_user' => $request->id_user,
                'mr' => $request->mr,
                'tanggal' => $request->tanggal,
                'biaya' => $request->biaya,
                'nama_pasien' => $request->nama_pasien,
                'status' => $request->status,
                'jam' => $activity['jam'],
                'id_skp' => $activity['id_skp'],
                'deskripsi' => $activity['deskripsi'],
                'lampiran' => $lampiran,
            ]);
        }
        return ['message' => "success"];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        return $activity;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activity $activity)
    {
        // dd($request);
        if ($request->base64) {
            $file = $request->base64;
            $name = time().'.'.explode('/', explode(':', substr($file, 0, strpos($file, ';')))[1])[1];

            $path = public_path('kegiatan/'.date("dmY").'/');
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }

            \Image::make($file)->resize(2000, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.$name);
            $request->lampiran = '/kegiatan/'.date("dmY").'/'.$name;

            if(file_exists(public_path($request->link))){
                @unlink(public_path($request->link));
            }
        }else{
            $request->lampiran = $request->link;
        }

        $activity->update([
            'id_user' => $request->id_user,
            'mr' => $request->mr,
            'tanggal' => $request->tanggal,
            'biaya' => $request->biaya,
            'nama_pasien' => $request->nama_pasien,
            'status' => $request->status,
            'jam' => $request->jam,
            'mutu' => $request->mutu,
            'id_skp' => $request->id_skp,
            'deskripsi' => $request->deskripsi,
            'lampiran' => $request->lampiran,
        ]);

        $updated = Activity::where('id', '=', $activity->id)
                   ->with([ 'skp:id,id_tupoksi', 'skp.tupoksi:id,butir_kegiatan' ])
                   ->first();

        if ($request->status !== 'Menunggu') {
            $this->assesment($request);
        }
        return ['message' => "success", 'updated' => $updated];
    }

    public function assesment(Request $request)
    {
        $tahun = date('Y', strtotime($request->tanggal));

        $activity = Activity::where('id_user', '=', $request->id_user)
                    ->where('id_skp', '=', $request->id_skp)
                    ->where('status', '=', 'Disetujui')
                    ->whereYear('tanggal', '=', $tahun);

        $count = $activity->count();
        $average = ceil($activity->avg('mutu'));
        $firstmonth = Carbon::parse($tahun.'-01-01');
        $lastmonth = Carbon::parse($activity->orderBy('tanggal', 'desc')->pluck('tanggal')->first());
        $waktu = $firstmonth->diffInMonths($lastmonth)+1;
        $hitungBiaya = $activity->sum('biaya');
        $biaya = $hitungBiaya == 0 ? null : $hitungBiaya;

        $penghitungan = $this->penghitungan($request->id_skp, $count, $average, $waktu, $biaya);

        return $penghitungan;
    }
    public function penghitungan($id, $c, $a, $w, $b)
    {
        $skp = Skp::findOrFail($id);
        $t_mutu = 100;
        $persenWaktu = 100 - ($w / $skp->t_waktu * 100 );
        $persenBiaya = $skp->t_biaya ? 100 - ($b / $skp->t_biaya * 100 ) : null ;
        $persenQty = ($c / $skp->t_qty) * 100;
        $persenMutu = ($a / $t_mutu) * 100;

        if( $persenWaktu > 24) {
            $waktu = 76 - ((((1.76 * $skp->t_waktu - $w) / $skp->t_waktu ) * 100 ) - 100);
        } else {
            $waktu = ((1.76 * $skp->t_waktu - $w ) / $skp->t_waktu ) * 100;
        }

        if($persenBiaya > 24 && $persenBiaya) {
            $biaya = 76 - ((((1.76 * $skp->t_biaya - $b ) / $skp->t_biaya ) * 100) - 100);
        } else if($persenBiaya < 24 && $persenBiaya) {
            $biaya = ((1.76 * $skp->t_biaya - $b ) / $skp->t_biaya ) * 100;
        } else {
            $biaya = 0;
        }

        $penghitungan = $persenQty + $persenMutu + $biaya + $waktu;
        $bagi = $b ? 4 : 3 ;
        $capaian = $penghitungan / $bagi;

        $skp->update([
            'r_qty' => $c,
            'r_mutu' => $a,
            'r_waktu' => $w,
            'r_biaya' => $b,
            'penghitungan' => $penghitungan,
            'capaian' => $capaian,
        ]);

        return $capaian;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        if(file_exists(public_path($activity->lampiran))){
            @unlink(public_path($activity->lampiran));
        }
        $activity->delete();
        return ['message' => 'Delete Success'];
    }
}
