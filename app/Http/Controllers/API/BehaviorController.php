<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Behavior;
use Illuminate\Http\Request;
use stdClass;

class BehaviorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getByUser(Request $request){
        $empty = null;
        $result = Behavior::where('id_user', '=', $request->id_user)->where('tahun', '=', $request->year)
        ->select('id', 'id_user', 'pelayanan', 'integritas', 'komitmen', 'disiplin', 'kerjasama', 'kepemimpinan', 'average')
        ->first();
        return $result ? $result : $empty;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postId = Behavior::insertGetId([
            'id_user' => $request['id_user'],
            'pelayanan' => $request['pelayanan'],
            'integritas' => $request['integritas'],
            'komitmen' => $request['komitmen'],
            'disiplin' => $request['disiplin'],
            'kerjasama' => $request['kerjasama'],
            'kepemimpinan' => $request['kepemimpinan'],
            'average' => $request['average'],
            'tahun' => $request['tahun'],
        ]);

        $newPost = Behavior::where('id', '=', $postId)
        ->select('id', 'id_user', 'pelayanan', 'integritas', 'komitmen', 'disiplin', 'kerjasama', 'kepemimpinan', 'average')
        ->first();

        return ['message' => 'created', 'newPost' => $newPost ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Behavior  $behavior
     * @return \Illuminate\Http\Response
     */
    public function show(Behavior $behavior)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Behavior  $behavior
     * @return \Illuminate\Http\Response
     */
    public function edit(Behavior $behavior)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Behavior  $behavior
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Behavior $behavior)
    {
        $behavior->update($request->all());

        $newPost = Behavior::where('id', '=', $behavior->id)
        ->select('id', 'id_user', 'pelayanan', 'integritas', 'komitmen', 'disiplin', 'kerjasama', 'kepemimpinan', 'average')
        ->first();

        return ['message' => 'updated', 'newPost' => $newPost];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Behavior  $behavior
     * @return \Illuminate\Http\Response
     */
    public function destroy(Behavior $behavior)
    {
        //
    }
}
