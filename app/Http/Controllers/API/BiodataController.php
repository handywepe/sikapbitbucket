<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Biodata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class BiodataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getBiodataUser($id)
    {
        $bio =  Biodata::where('id_user', '=', $id)->first();
        return $bio;
    }

    public function countGender()
    {
        $l =  Biodata::where('gender', '=', 'Laki-laki')->count();
        $p =  Biodata::where('gender', '=', 'Perempuan')->count();
        return ['l' => $l, 'p' => $p];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Biodata::create($request->all());
        return ['message' => "success"];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Biodata  $biodata
     * @return \Illuminate\Http\Response
     */
    public function show(Biodata $biodata)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Biodata  $biodata
     * @return \Illuminate\Http\Response
     */
    public function edit(Biodata $biodata)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Biodata  $biodata
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Biodata $biodata)
    {

        $biodata = Biodata::findOrFail($request->id);

        $biodata->update($request->all());
        return ['message' => "success"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Biodata  $biodata
     * @return \Illuminate\Http\Response
     */
    public function destroy(Biodata $biodata)
    {
        //
    }

    public function Birthday()
    {
        $day = Date('d');
        $month = Date ('m');
        $birthday = Biodata::whereDay('tgl_lahir', '=', $day)
                    ->whereMonth('tgl_lahir', '=', $month)
                    ->leftJoin('users', 'users.id', '=', 'biodata.id_user')
                    ->where('users.active', '=', 1)
                    ->select('id_user')
                    ->with(['user:id,fullname,photo', 'user.penempatan:id_user,nama_unit'])
                    ->get();
        return $birthday;
    }
}
