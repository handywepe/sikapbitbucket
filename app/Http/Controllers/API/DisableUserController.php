<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\DisableUser;
use Illuminate\Http\Request;
use App\Models\User;

class DisableUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return User::where('active', '=', 0)
        // ->with('address', 'academic:id_user,last_acad,last_jurusan', 'disable', 'biodata', 'penempatan')
        ->select('id', 'nip', 'fullname', 'photo')
        ->with('disable')
        ->orderBy('updated_at', 'desc')->get();

        // return DisableUser::with('user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DisableUser::create([
            'id_user' => $request->id,
            'tmt_keluar' => $request->tmt_keluar,
            'alasan_keluar' => $request->alasan_keluar,
        ]);

        $user = User::findOrFail($request->id);
        $user->update([
            'active' => 0,
        ]);

        return ['message' => 'Disable User Success'];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DisableUser  $disableUser
     * @return \Illuminate\Http\Response
     */
    public function show(DisableUser $disableUser)
    {
        return $disableUser->load('user.placement', 'user.biodata', 'user.academic', 'user.address');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DisableUser  $disableUser
     * @return \Illuminate\Http\Response
     */
    public function edit(DisableUser $disableUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DisableUser  $disableUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DisableUser $disableUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DisableUser  $disableUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(DisableUser $disableUser)
    {
        //
    }
}
