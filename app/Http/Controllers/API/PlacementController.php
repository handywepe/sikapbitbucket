<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Placement;
use Illuminate\Http\Request;
use App\Models\UnitStructures;

class PlacementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getAtasan(Request $request)
    {
        //
        if ($request->level == 0) {
            $levelPenilai = 0.5;
            $levelAtasan = 5.5;
            $bagianPenilai = 'id_unit';
            $bagianAtasan = 'sub_bagian';
        }elseif ($request->level == 0.5 || $request->level == 1.5) {
            $levelPenilai = 5.5;
            $levelAtasan = 6.5;
            $bagianPenilai = 'sub_bagian';
            $bagianAtasan = 'bagian';
        }elseif ($request->level == 1) {
            $levelPenilai = 1.5;
            $levelAtasan = 5.5;
            $bagianPenilai = 'id_unit';
            $bagianAtasan = 'sub_bagian';
        }elseif ($request->level == 2) {
            $levelPenilai = 2.5;
            $levelAtasan = 3.5;
            $bagianPenilai = 'id_unit';
            $bagianAtasan = 'instalasi';
        }elseif ($request->level == 2.5 || $request->level == 3) {
            $levelPenilai = 3.5;
            $levelAtasan = 5.5;
            $bagianPenilai = 'instalasi';
            $bagianAtasan = 'sub_bagian';
        }elseif ($request->level == 3.5 || $request->level == 5) {
            $levelPenilai = 5.5;
            $levelAtasan = 6.5;
            $bagianPenilai = 'sub_bagian';
            $bagianAtasan = 'bagian';
        }elseif ($request->level == 5.5 || $request->level == 6) {
            $levelPenilai = 6.5;
            $levelAtasan = 7;
            $bagianPenilai = 'bagian';
            $bagianAtasan = 'wadir';
        }elseif ( $request->level == 6.5){
            $levelPenilai = 7;
            $levelAtasan = 8;
            $bagianPenilai = 'wadir';
            $bagianAtasan = 'wadir';
        }else if( $request->level == 7){
            $levelPenilai = 8;
            $levelAtasan = null;
            $bagianPenilai = 'wadir';
            $bagianAtasan = 'wadir';
        }else{
            $levelPenilai = null;
            $levelAtasan = null;
            $bagianPenilai = 'wadir';
            $bagianAtasan = 'wadir';
        }

        $structure = UnitStructures::where('id_unit', '=', $request->id_unit)->first();
        if ($request->level <= 6 ) {
            $penilai = Placement::where('id_unit', '=', $structure->$bagianPenilai)
                        ->leftJoin('positions', 'positions.id', '=', 'placements.id_position')
                        ->select('placements.*', 'positions.level')
                        ->where('positions.level', '=', $levelPenilai)
                        ->with('user:id,fullname,nip,gol', 'position:id,jabatan', 'unit:id,nama_unit')
                        ->first();

            $atasanPenilai = Placement::where('id_unit', '=', $structure->$bagianAtasan)
                            ->leftJoin('positions', 'positions.id', '=', 'placements.id_position')
                            ->select('placements.*', 'positions.level')
                            ->where('positions.level', '=', $levelAtasan)
                            ->with('user:id,fullname,nip,gol', 'position:id,jabatan', 'unit:id,nama_unit')
                            ->first();
        } else {
            $penilai = Placement::where('id_unit', '=', $structure->$bagianPenilai)
                        ->leftJoin('positions', 'positions.id', '=', 'placements.id_position')
                        ->select('placements.*', 'positions.level')
                        ->where('positions.level', '=', $levelPenilai)
                        ->with('user:id,fullname,nip,gol', 'position:id,jabatan', 'unit:id,nama_unit')
                        ->first();

            $atasanPenilai = Placement::where('id_unit', '=', 1)
                            ->leftJoin('positions', 'positions.id', '=', 'placements.id_position')
                            ->select('placements.*', 'positions.level')
                            ->where('positions.level', '=', $levelAtasan)
                            ->with('user:id,fullname,nip,gol', 'position:id,jabatan', 'unit:id,nama_unit')
                            ->first();
        }

        $value = [ 'penilai' => $penilai, 'atasanPenilai' => $atasanPenilai];
        return $value;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Placement  $placement
     * @return \Illuminate\Http\Response
     */
    public function show(Placement $placement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Placement  $placement
     * @return \Illuminate\Http\Response
     */
    public function edit(Placement $placement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Placement  $placement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Placement $placement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Placement  $placement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Placement $placement)
    {
        //
        $placement->delete();
        return ['message' => 'Delete Success'];
    }
}
