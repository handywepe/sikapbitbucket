<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Position::all();
    }
    public function showJab()
    {
        //show all
        // if (\Gate::allows('isSuperAdmin') || \Gate::allows('isSys') || \Gate::allows('isAdmin')){
            $internal = Position::where('jenis_jabatan', '=', 'Internal')->select('id', 'jabatan')->get();
            $permenpan = Position::where('jenis_jabatan', '!=', 'Internal')->select('id', 'jabatan')->get();
        // }
        return ['internal'=>$internal,'permenpan'=>$permenpan ];
    }

    public function showJft()
    {
        return Position::where('jenis_jabatan', '=', 'jft')->get();
    }

    public function showJfu()
    {
        return Position::where('jenis_jabatan', '=', 'jfu')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //tambah
        $this->validate($request, [
            'jabatan' => 'required|string|max:191|unique:positions',
        ]);

        $posId = Position::insertGetId([
            'jabatan' => $request['jabatan'],
            'jenis_jabatan' => $request['jenis_jabatan'],
            'kategori' => $request['kategori'],
            'level' => $request['level'],
            'shifting' => $request['shifting'],
        ]);

        $newPos = Position::where('id', '=', $posId)->first();

        return ['message' => 'Success', 'newPos' => $newPos ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function show(Position $position)
    {
        return $position;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Position $position)
    {
        //edit

        $this->validate($request, [
            'jabatan' => 'required|string|max:255|unique:positions,jabatan,'.$position->id,
        ]);

        $position->update($request->all());

        $updatePos = $position;

        return ['message' => 'Success', 'updatePos' => $updatePos];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function destroy(Position $position)
    {
        $position->delete();
        return ['message' => 'Delete Success'];
    }
}
