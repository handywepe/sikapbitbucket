<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Behavior;
use App\Models\Skp;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SkpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_user' => 'required',
            'id_tupoksi' => [
                'required',
                Rule::unique('skps')->where(function ($query) use ($request) {
                    return $query->where('id_user', $request->id_user);
                })
            ],
        ]);

        $lastId = Skp::insertGetId([
            'id_user' => $request['id_user'],
            'id_tupoksi' => $request['id_tupoksi'],
            't_waktu' => $request['t_waktu'],
            't_qty' => $request['t_qty'],
            't_biaya' => $request['t_biaya'],
            'tahun' => $request['tahun'],
        ]);

        $created = Skp::where('id', '=', $lastId)->with('tupoksi:id,satuan_hasil,butir_kegiatan,angka_kredit,qty')->first();

        return ['message' => 'Create Success', 'created' => $created];
    }

    public function skpPegawai(Request $request)
    {
        $user = auth('sanctum')->user();
        $skp = Skp::where('tahun', '=', $request->year)
                ->where('id_user', '=', $user->id)
                ->with('tupoksi:id,satuan_hasil,butir_kegiatan,angka_kredit,qty')
                ->get();
        return $skp;
    }

    public function getSkpPegawai(Request $request)
    {
        $skp = Skp::where('tahun', '=', $request->year)
                ->where('id_user', '=', $request->id_user)
                ->with('tupoksi:id,satuan_hasil,butir_kegiatan,angka_kredit,qty')
                ->get();
        return $skp;
    }

    public function getPrestasi( Request $request)
    {
        $skp = round(Skp::where('tahun', '=', $request->year)
                    ->where('id_user', '=', $request->id_user)
                    ->avg('capaian'), 2);
        $behavior = Behavior::where('tahun', '=', $request->year)
                    ->where('id_user', '=', $request->id_user)
                    ->select('pelayanan', 'integritas', 'komitmen', 'disiplin', 'kerjasama', 'kepemimpinan', 'average')
                    ->first();

        return [ 'capaian' => $skp, 'perilaku' => $behavior ];

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Skp  $skp
     * @return \Illuminate\Http\Response
     */
    public function show(Skp $skp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Skp  $skp
     * @return \Illuminate\Http\Response
     */
    public function edit(Skp $skp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Skp  $skp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Skp $skp)
    {
        $this->validate($request, [
            'id_user' => 'required',
            'id_tupoksi' => [
                'required',
                Rule::unique('skps')->where(function ($query) use ($request) {
                    return $query->where('id_user', $request->id_jabatan);
                })->ignore($request->id)
            ],
        ]);

        $skp->update($request->all());

        $updated = $skp->with('tupoksi:id,satuan_hasil,butir_kegiatan,angka_kredit,qty')->first();

        return ['message' => "success", 'updated' => $updated];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Skp  $skp
     * @return \Illuminate\Http\Response
     */
    public function destroy(Skp $skp)
    {
        $skp->delete();
        return ['message' => 'Tupoksi Deleted'];
    }
}
