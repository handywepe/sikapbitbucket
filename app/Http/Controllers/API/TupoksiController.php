<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Tupoksi;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TupoksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth('sanctum')->user()->load('placement');
        $result = [];
        foreach ($user->placement as $placement) {
            $tupoksis = Tupoksi::where('id_jabatan', '=', $placement->id_permenpan)->select('id', 'butir_kegiatan')->get();
            foreach($tupoksis as $tupoksi){
                array_push($result, $tupoksi);
            }
        }
        return $result;
    }
    public function tupoksiByLogin(Request $request)
    {
        $login = auth('api')->user();

        $jafung = DB::table('tupoksis')
        ->where('id_jabatan', '=', $login->id_jafung)
        ->get();

        $jabpns = DB::table('tupoksis')
        ->where('id_jabatan', '=', $login->id_jabpns)
        ->get();

        return ['jafung'=>$jafung, 'jabpns'=>$jabpns];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function tupoksiJabatan(Request $request)
    {
        //
        return Tupoksi::where('id_jabatan', '=', $request->id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_jabatan' => 'required',
            'butir_kegiatan' => [
                'required',
                Rule::unique('tupoksis')->where(function ($query) use ($request) {
                    return $query->where('id_jabatan', $request->id_jabatan);
                //    ->where('butir_kegiatan', $request->butir_kegiatan);
                })
            ],
        ]);

        $lastId = Tupoksi::insertGetId([
            'id_jabatan' => $request['id_jabatan'],
            'butir_kegiatan' => $request['butir_kegiatan'],
            'angka_kredit' => $request['angka_kredit'],
            'satuan_hasil' => $request['satuan_hasil'],
            'qty' => $request['qty'],
        ]);

        $created = Tupoksi::where('id', '=', $lastId)->first();

        return ['message' => 'Create Success', 'created' => $created];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tupoksi  $tupoksi
     * @return \Illuminate\Http\Response
     */
    public function show(Tupoksi $tupoksi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tupoksi  $tupoksi
     * @return \Illuminate\Http\Response
     */
    public function edit(Tupoksi $tupoksi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tupoksi  $tupoksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tupoksi $tupoksi)
    {
        //
        $this->validate($request, [
            'id_jabatan' => 'required',
            'butir_kegiatan' => [
                'required',
                Rule::unique('tupoksis')->where(function ($query) use ($request) {
                    return $query->where('id_jabatan', $request->id_jabatan);
                //    ->where('butir_kegiatan', $request->butir_kegiatan);
                })->ignore($request->id)
            ],
        ]);
        $tupoksi->update($request->all());

        $updated = $tupoksi;

        return ['message' => "success", 'updated' => $updated];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tupoksi  $tupoksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tupoksi $tupoksi)
    {
        //
        $tupoksi->delete();
        return ['message' => 'Tupoksi Deleted'];
    }
}
