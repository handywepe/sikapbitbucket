<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Unit;
use App\Models\UnitStructures;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //show all
            return Unit::all();
    }

    public function selectUnit()
    {
        return Unit::select('id', 'nama_unit')->get();
    }

    public function allUnit()
    {
        //show all
        return Unit::select('id AS id_unit', 'nama_unit')->get();
    }

    public function unitPelayanan()
    {
        return Unit::where('pelayanan', '=', '1')->select('id', 'nama_unit')->get();
    }

    public function allShiftUnit()
    {
        //show all
        return Unit::where('shifting_unit', '=', 'shift')->select('id', 'nama_unit')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //tambah
        $this->validate($request, [
            'nama_unit' => 'unique:units',
        ]);

        $unitId = Unit::insertGetId([
            'nama_unit' => $request['nama_unit'],
            'bagian' => $request['bagian'],
            'shifting_unit' => $request['shifting_unit'],
            'pelayanan' => $request['pelayanan'],
        ]);

        $newUnit = Unit::where('id', '=', $unitId)->first();;

        return [ 'message' => 'success' , 'newUnit' => $newUnit ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        //
        return $unit;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {
        //edit
        $this->validate($request, [
            'nama_unit' => 'unique:units,nama_unit,'.$unit->id,
        ]);

        $unit->update($request->all());

        $updateUnit = $unit;

        return [ 'message' => 'success' , 'updateUnit' => $updateUnit ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit)
    {
        $unit->delete();
        return ['message' => 'Unit Deleted'];
    }

    public function unitByPlacement()
    {
        $user = auth('sanctum')->user()->load('placement');
        $result = [];
        foreach($user->placement as $placement){
            if ($placement->level == 3.5 || $placement->level == 4.5) {
                $units = $this->unitByInstalasi($placement->id_unit);
                foreach ($units as $unit) {
                    if ($unit !== null) {
                        array_push($result, $unit);
                    }
                }
            }else if ($placement->level == 5.5) {
                $units = $this->unitBySubBagian($placement->id_unit);
                foreach ($units as $unit) {
                    if ($unit !== null) {
                        array_push($result, $unit);
                    }
                }
            }else if ($placement->level == 6.5) {
                $units = $this->unitByBagian($placement->id_unit);
                foreach ($units as $unit) {
                    if ($unit !== null) {
                        array_push($result, $unit);
                    }
                }
            }else if ($placement->level == 7) {
                $units = $this->unitByWadir($placement->id_unit);
                foreach ($units as $unit) {
                    if ($unit !== null) {
                        array_push($result, $unit);
                    }
                }
            }
        }
        return $result;
    }

    public function unitByInstalasi($unit)
    {
        $unit = UnitStructures::where('instalasi', '=', $unit)
                ->leftJoin('units', 'units.id', '=', 'unit_structures.id_unit')
                ->select('unit_structures.id_unit', 'units.nama_unit')
                ->get();
        return $unit;
    }

    public function unitBySubBagian($unit)
    {
        $unit = UnitStructures::where('sub_bagian', '=', $unit)
                ->leftJoin('units', 'units.id', '=', 'unit_structures.id_unit')
                ->select('unit_structures.id_unit', 'units.nama_unit')
                ->get();
        return $unit;
    }

    public function unitByBagian($unit)
    {
        $unit = UnitStructures::where('bagian', '=', $unit)
                ->leftJoin('units', 'units.id', '=', 'unit_structures.id_unit')
                ->select('unit_structures.id_unit', 'units.nama_unit')
                ->get();
        return $unit;
    }

    public function unitByWadir($unit)
    {
        $unit = UnitStructures::where('wadir', '=', $unit)
                ->leftJoin('units', 'units.id', '=', 'unit_structures.id_unit')
                ->select('unit_structures.id_unit', 'units.nama_unit')
                ->get();
        return $unit;
    }

    public function unitShiftByInstalasi(Request $request)
    {
        $unit = UnitStructures::join('units', 'units.id', '=', 'unit_structures.id_unit')
        ->where('instalasi', '=', $request->kains)
        ->where('units.shifting_unit', '=', 'shift')
        ->select('units.id', 'units.nama_unit')->get();
        return $unit;
    }

    public function unitShiftByKomite(Request $request)
    {
        $unit = UnitStructures::join('units', 'units.id', '=', 'unit_structures.id_unit')
        ->where('instalasi', '=', $request->kakomite)
        ->where('units.shifting_unit', '=', 'shift')
        ->select('units.id', 'units.nama_unit')->get();
        return $unit;
    }

    public function unitShiftBySubBagian(Request $request)
    {
        $unit = UnitStructures::join('units', 'units.id', '=', 'unit_structures.id_unit')
        ->where('sub_bagian', '=', $request->kasubag)
        ->where('units.shifting_unit', '=', 'shift')
        ->select('units.id', 'units.nama_unit')->get();
        return $unit;
    }

    public function unitShiftByBagian(Request $request)
    {
        $unit = UnitStructures::join('units', 'units.id', '=', 'unit_structures.id_unit')
        ->where('unit_structures.bagian', '=', $request->kabag)
        ->where('units.shifting_unit', '=', 'shift')
        ->select('units.id', 'units.nama_unit')->get();
        return $unit;
    }

    public function unitShiftByWadir(Request $request)
    {
        $unit = UnitStructures::join('units', 'units.id', '=', 'unit_structures.id_unit')
        ->where('wadir', '=', $request->wadir)
        ->where('units.shifting_unit', '=', 'shift')
        ->select('units.id', 'units.nama_unit')->get();
        return $unit;
    }
}
