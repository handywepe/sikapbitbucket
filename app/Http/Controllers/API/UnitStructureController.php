<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\UnitStructures;
use Illuminate\Http\Request;

class UnitStructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UnitStructures  $unitStructures
     * @return \Illuminate\Http\Response
     */
    public function show(UnitStructures $unitStructures)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UnitStructures  $unitStructures
     * @return \Illuminate\Http\Response
     */
    public function edit(UnitStructures $unitStructures)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UnitStructures  $unitStructures
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UnitStructures $unitStructures)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UnitStructures  $unitStructures
     * @return \Illuminate\Http\Response
     */
    public function destroy(UnitStructures $unitStructures)
    {
        //
    }
}
