<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Academic;
use App\Models\Biodata;
use App\Models\Placement;
use App\Models\Address;
use App\Models\Behavior;
use App\Models\UnitStructures;
use App\Models\Activity;
use App\Models\Skp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Collection;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('active', '=', 1)
        ->orderBy('users.id', 'asc')
        ->leftjoin('biodata', 'biodata.id_user', '=', 'users.id')
        ->select('users.id', 'users.fullname', 'users.nip', 'users.tmt', 'users.photo', 'users.jenis_peg', 'biodata.gender', 'users.lapbul')
        ->with('penempatan')
        ->get();

        return $user;
    }

    public function canAccessUser()
    {
        $user = auth('sanctum')->user()->load('placement');
        $result = [];
        foreach ($user->placement as $placement) {
            if ($placement->level == 7.0) {
                $structures = UnitStructures::where('wadir', '=', $placement->id_unit)->select('id_unit')->pluck('id_unit');
                foreach ($structures as $structure) {
                    $placements = Placement::where('id_unit', '=', $structure)->select('id_user')->pluck('id_user');
                    foreach ($placements as $placement) {
                        $users = User::where('id', '=', $placement)->where('active', '=', 1)
                                ->orderBy('id', 'asc')->pluck('id')->first();
                        if($users !== null){
                            array_push($result, $users);
                        }
                    }
                }
            }
            else if ($placement->level == 6.5) {
                $structures = UnitStructures::where('bagian', '=', $placement->id_unit)->select('id_unit')->pluck('id_unit');
                foreach ($structures as $structure) {
                    $placements = Placement::where('id_unit', '=', $structure)->select('id_user')->pluck('id_user');
                    foreach ($placements as $placement) {
                        $users = User::where('id', '=', $placement)->where('active', '=', 1)
                                ->orderBy('id', 'asc')->pluck('id')->first();
                        if($users !== null){
                            array_push($result, $users);
                        }
                    }
                }
            }
            else if ($placement->level == 5.5 || $placement->level == 5) {
                $structures = UnitStructures::where('sub_bagian', '=', $placement->id_unit)->select('id_unit')->pluck('id_unit');
                foreach ($structures as $structure) {
                    $placements = Placement::where('id_unit', '=', $structure)->select('id_user')->pluck('id_user');
                    foreach ($placements as $placement) {
                        $users = User::where('id', '=', $placement)->where('active', '=', 1)
                                ->orderBy('id', 'asc')->pluck('id')->first();
                        if($users !== null){
                            array_push($result, $users);
                        }
                    }
                }
            }
            else if ($placement->level == 3.5) {
                $structures = UnitStructures::where('instalasi', '=', $placement->id_unit)->select('id_unit')->pluck('id_unit');
                foreach ($structures as $structure) {
                    $placements = Placement::where('id_unit', '=', $structure)->select('id_user')->pluck('id_user');
                    foreach ($placements as $placement) {
                        $users = User::where('id', '=', $placement)->where('active', '=', 1)
                                ->orderBy('id', 'asc')->pluck('id')->first();
                        if($users !== null){
                            array_push($result, $users);
                        }
                    }
                }
            }
            else if ($placement->level == 4.5 || $placement->level == 2.5 || $placement->level == 1.5) {
                $placements = Placement::where('id_unit', '=', $placement->id_unit)->select('id_user')->pluck('id_user');
                foreach ($placements as $placement) {
                    $users = User::where('id', '=', $placement)->where('active', '=', 1)
                            ->orderBy('id', 'asc')->pluck('id')->first();
                    if($users !== null){
                        array_push($result, $users);
                    }
                }
            }
        }
        return $result;
    }

    public function userByStructure(){
        $user = auth('sanctum')->user()->load('placement');
        $result = [];
        foreach ($user->placement as $placement) {
            if ($placement->level == 7.0) {
                $structures = UnitStructures::where('wadir', '=', $placement->id_unit)->select('id_unit')->pluck('id_unit');
                foreach ($structures as $structure) {
                    $placements = Placement::where('id_unit', '=', $structure)->select('id_user')->pluck('id_user');
                    foreach ($placements as $placement) {
                        $users = User::where('id', '=', $placement)->where('active', '=', 1)->select('id', 'fullname', 'nip', 'tmt', 'gol')
                                ->orderBy('users.id', 'asc')->with('penempatan')->first();
                        if($users !== null){
                            array_push($result, $users);
                        }
                    }
                }
            }
            else if ($placement->level == 6.5) {
                $structures = UnitStructures::where('bagian', '=', $placement->id_unit)->select('id_unit')->pluck('id_unit');
                foreach ($structures as $structure) {
                    $placements = Placement::where('id_unit', '=', $structure)->select('id_user')->pluck('id_user');
                    foreach ($placements as $placement) {
                        $users = User::where('id', '=', $placement)->where('active', '=', 1)->select('id', 'fullname', 'nip', 'tmt', 'gol')
                                ->with('penempatan')->first();
                        if($users !== null){
                            array_push($result, $users);
                        }
                    }
                }
            }
            else if ($placement->level == 5.5 || $placement->level == 5) {
                $structures = UnitStructures::where('sub_bagian', '=', $placement->id_unit)->select('id_unit')->pluck('id_unit');
                foreach ($structures as $structure) {
                    $placements = Placement::where('id_unit', '=', $structure)->select('id_user')->pluck('id_user');
                    foreach ($placements as $placement) {
                        $users = User::where('id', '=', $placement)->where('active', '=', 1)->select('id', 'fullname', 'nip', 'tmt', 'gol')
                                ->orderBy('users.id', 'asc')->with('penempatan')->first();
                        if($users !== null){
                            array_push($result, $users);
                        }
                    }
                }
            }
            else if ($placement->level == 3.5) {
                $structures = UnitStructures::where('instalasi', '=', $placement->id_unit)->select('id_unit')->pluck('id_unit');
                foreach ($structures as $structure) {
                    $placements = Placement::where('id_unit', '=', $structure)->select('id_user')->pluck('id_user');
                    foreach ($placements as $placement) {
                        $users = User::where('id', '=', $placement)->where('active', '=', 1)->select('id', 'fullname', 'nip', 'tmt', 'gol')
                                ->orderBy('users.id', 'asc')->with('penempatan')->first();
                        if($users !== null){
                            array_push($result, $users);
                        }
                    }
                }
            }
            else if ($placement->level == 4.5 || $placement->level == 2.5 || $placement->level == 1.5) {
                $placements = Placement::where('id_unit', '=', $placement->id_unit)->select('id_user')->pluck('id_user');
                foreach ($placements as $placement) {
                    $users = User::where('id', '=', $placement)->where('active', '=', 1)->select('id', 'fullname', 'nip', 'tmt', 'gol')
                    ->orderBy('users.id', 'asc')->with('penempatan')->first();
                    if($users !== null){
                        array_push($result, $users);
                    }
                }
            }
        }
        return $result;
    }

    public function userAssesment()
    {
        $user = auth('sanctum')->user()->load('placement');
        if($user->role != 'administrator'){
            // $result = [];
            $result = new Collection();
            foreach ($user->placement as $placement) {
                if ($placement->level == 0.5) {
                    $users = Placement::where('id_unit', '=', $placement->id_unit)
                            ->rightjoin('positions','positions.id', '=', 'placements.id_position')
                            ->join('users', 'users.id', '=', 'placements.id_user')
                            ->select('placements.*', 'positions.level', 'users.fullname', 'users.active', 'users.photo')
                            ->where('users.active', '=', 1)
                            ->where('positions.level', '=', 0)
                            ->with('unit:id,nama_unit')
                            ->get();
                    // foreach ($users as $user) {
                    //     if ($user !== null) { array_push($result, $user); }
                    // }
                    $result = $result->merge($users);
                } elseif ($placement->level == 1.5) {
                    $users = Placement::where('id_unit', '=', $placement->id_unit)
                            ->rightjoin('positions','positions.id', '=', 'placements.id_position')
                            ->join('users', 'users.id', '=', 'placements.id_user')
                            ->select('placements.*', 'positions.level', 'users.fullname', 'users.active', 'users.photo')
                            ->where('users.active', '=', 1)
                            ->with('unit:id,nama_unit')
                            ->get();
                    $result = $result->merge($users);
                } elseif ($placement->level == 2.5) {
                    if($placement->id_unit === 47){
                        $users = Placement::where(function($query){
                                    $query->where('id_unit', '=', 47)->orWhere('id_unit', '=', 81);
                                })
                                ->rightjoin('positions','positions.id', '=', 'placements.id_position')
                                ->join('users', 'users.id', '=', 'placements.id_user')
                                ->select('placements.*', 'positions.level', 'users.fullname', 'users.active', 'users.photo')
                                ->where('users.active', '=', 1)
                                ->where('positions.level', '=', 2)
                                ->with('unit:id,nama_unit')
                                ->get();
                        $result = $result->merge($users);
                    }else {
                        $users = Placement::where('id_unit', '=', $placement->id_unit)
                                ->rightjoin('positions','positions.id', '=', 'placements.id_position')
                                ->join('users', 'users.id', '=', 'placements.id_user')
                                ->select('placements.*', 'positions.level', 'users.fullname', 'users.active', 'users.photo')
                                ->where('users.active', '=', 1)
                                ->where('positions.level', '=', 2)
                                ->with('unit:id,nama_unit')
                                ->get();
                        $result = $result->merge($users);
                    }
                } elseif ($placement->level === 3.5) {
                    $units = UnitStructures::where('instalasi', '=', $placement->id_unit)->get();
                    foreach ($units as $unit) {
                        $users = Placement::where('id_unit', '=', $unit->id_unit)
                                ->rightjoin('positions','positions.id', '=', 'placements.id_position')
                                ->join('users', 'users.id', '=', 'placements.id_user')
                                ->select('placements.*', 'positions.level', 'users.fullname', 'users.active', 'users.photo')
                                ->where('users.active', '=', 1)
                                ->where(function ($query) {
                                    $query->where('positions.level', '=', 2.5)->orWhere('positions.level', '=', 3);
                                })
                                ->with('unit:id,nama_unit')
                                ->get();
                        $result = $result->merge($users);
                    }
                } elseif ($placement->level === 4.5) {
                        $users = Placement::where('id_unit', '=', $placement->id_unit)
                            ->rightjoin('positions','positions.id', '=', 'placements.id_position')
                            ->join('users', 'users.id', '=', 'placements.id_user')
                            ->select('placements.*', 'positions.level', 'users.fullname', 'users.active', 'users.photo')
                            ->where('users.active', '=', 1)
                            ->where('positions.level', '=', 4)
                            ->with('unit:id,nama_unit')
                            ->get();
                        $result = $result->merge($users);
                } elseif ($placement->level === 5.5) {
                    $units = UnitStructures::where('sub_bagian', '=', $placement->id_unit)->get();
                    foreach($units as $unit){
                        $users = Placement::where('id_unit', '=', $unit->id_unit)
                                ->rightjoin('positions','positions.id', '=', 'placements.id_position')
                                ->join('users', 'users.id', '=', 'placements.id_user')
                                ->select('placements.*', 'positions.level', 'users.fullname', 'users.active', 'users.photo')
                                ->where('users.active', '=', 1)
                                ->where(function ($query) {
                                    $query->where('positions.level', '=', 3.5)->orWhere('positions.level', '=', 5);
                                })
                                ->with('unit:id,nama_unit')
                                ->get();
                        $result = $result->merge($users);
                    }
                } elseif ($placement->level === 6.5) {
                    $units = UnitStructures::where('bagian', '=', $placement->id_unit)->get();
                    foreach($units as $unit){
                        $users = Placement::where('id_unit', '=', $unit->id_unit)
                                ->rightjoin('positions','positions.id', '=', 'placements.id_position')
                                ->join('users', 'users.id', '=', 'placements.id_user')
                                ->select('placements.*', 'positions.level', 'users.fullname', 'users.active', 'users.photo')
                                ->where('users.active', '=', 1)
                                ->where(function ($query) {
                                    $query->where('positions.level', '=', 5.5)->orWhere('positions.level', '=', 6);
                                })
                                ->with('unit:id,nama_unit')
                                ->get();
                        $result = $result->merge($users);
                    }
                } elseif ($placement->level === 7.0) {
                    $units = UnitStructures::where('wadir', '=', $placement->id_unit)->get();
                    foreach($units as $unit){
                        $users = Placement::where('id_unit', '=', $unit->id_unit)
                            ->rightjoin('positions','positions.id', '=', 'placements.id_position')
                            ->join('users', 'users.id', '=', 'placements.id_user')
                            ->select('placements.*', 'positions.level', 'users.fullname', 'users.active', 'users.photo')
                            ->where('users.active', '=', 1)
                            ->where('positions.level', '=', 6.5)
                            ->with('unit:id,nama_unit')
                            ->get();
                        $result = $result->merge($users);
                    }
                } elseif ($placement->level === 8.0) {
                    $units = UnitStructures::where('wadir', '=', $placement->id_unit)->get();
                    foreach($units as $unit){
                        $users = Placement::where('id_unit', '=', $unit->id_unit)
                                ->rightjoin('positions','positions.id', '=', 'placements.id_position')
                                ->join('users', 'users.id', '=', 'placements.id_user')
                                ->select('placements.*', 'positions.level', 'users.fullname', 'users.active', 'users.photo')
                                ->where('users.active', '=', 1)
                                ->where(function ($query) {
                                    $query->where('positions.level', '=', 7.0)->orWhere('positions.level', '=', 4.5);
                                })
                                ->with('unit:id,nama_unit')
                                ->get();
                        $result = $result->merge($users);
                    }
                }
            }
            return $result;
        }else{
            $result = User::where('active', '=', 1)
            ->rightJoin('placements', 'placements.id_user', '=', 'users.id')
            ->select('users.id', 'users.id as id_user', 'users.active', 'users.fullname', 'users.photo', 'placements.id_permenpan', 'placements.id_position', 'placements.id_unit',);

            return $result;
        }

    }

    public function countActivity(Request $request){
        $role = $user = auth('sanctum')->user()->role;
        $user = $this->userAssesment();
        $tahun = $request->year;
        $count = [];
        if ($role != "administrator") {
            foreach($user as $r){
                $accept = count(Activity::where('id_user', '=', $r->id_user)->whereYear('tanggal', '=', $tahun)
                                ->where('status', '=', 'Disetujui')->get());
                $denied = count(Activity::where('id_user', '=', $r->id_user)->whereYear('tanggal', '=', $tahun)
                                ->where('status', '=', 'Ditolak')->get());
                $Qbehavior = Behavior::where('id_user', '=', $r->id_user)->where('tahun', '=', $tahun)
                                ->select('id_user', 'average');
                $behavior = $Qbehavior->first();
                $avgBehavior = $Qbehavior->pluck('average')->first();
                $capaian = round(Skp::where('id_user', '=', $r->id_user)->where('tahun', '=', $tahun)->avg('capaian'), 2);
                $prestasi = round(($capaian * 60/100) + ($avgBehavior * 40/100), 2);
                $wait = count(Activity::where('id_user', '=', $r->id_user)->where('status', '=', 'Menunggu')->get());
                $push = [
                    'id_user' => $r->id_user,
                    'active' => $r->active,
                    'fullname' => $r->fullname,
                    'id' => $r->id,
                    'id_permenpan' => $r->id_permenpan,
                    'id_position' => $r->id_position,
                    'id_unit' => $r->id_unit,
                    'level' => $r->level,
                    'photo' => $r->photo,
                    'unit' => $r->unit,
                    'wait' => $wait,
                    'capaian' => $capaian,
                    'behavior' => $behavior,
                    // 'avgBehavior' => $avgBehavior,
                    'prestasi' => $prestasi,
                    'accept' => $accept,
                    'denied' => $denied,
                ];
                array_push($count, $push);
            }
            return $count;
        }else{
            return
            $user->withCount([
                'accept as accept' => function ($query) use ($tahun) {
                    $query->whereYear('tanggal', '=', $tahun);
                },
                'denied as denied' => function ($query) use ($tahun) {
                    $query->whereYear('tanggal', '=', $tahun);
                },
                'wait as wait' => function ($query) use ($tahun) {
                    $query->whereYear('tanggal', '=', $tahun);
                },

            ])
            ->with([
                'unit:id,nama_unit',
                'skp' => function ($query) use ($tahun){
                    $query->where('tahun', '=', $tahun);
                },
                'behavior' => function ($query) use ($tahun) {
                    $query->where('tahun', '=', $tahun)->select('id_user', 'average');
                },
            ])
            // ->without('behavior')
            ->get();
        }

    }

    // public function theBest()
    // {
    //     $user = $this->countActivity();
    //     $sortArray = array();

    //     foreach($user as $u){
    //         foreach($u as $key=>$value){
    //             if(!isset($sortArray[$key])){
    //                 $sortArray[$key] = array();
    //             }
    //             $sortArray[$key][] = $value;
    //         }
    //     }

    //     $orderby = "capaian"; //change this to whatever key you want from the array

    //     array_multisort($sortArray[$orderby],SORT_DESC,$user);

    //     return $user[0];
    // }

    public function userByUnit(Request $request)
    {
        $unit = $request->id_unit;
        $result = Placement::where('id_unit', '=', $unit )
            ->join('users', 'users.id', '=', 'placements.id_user')
            ->where('users.active', '=', 1)
            ->select('placements.*', 'users.fullname', 'users.photo')->with('unit')->get();
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function email(){
        $email = User::where('email', 'LIKE', "%user%")->orderBy('id', 'desc')->select(DB::raw('SUBSTR(email, 5) AS email'))->first();
        $number = substr($email, 10);
        $number2 = substr($number, 0, strpos($number, '@'));
        return $number2;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nip' => 'required|string|max:191|unique:users',
            // 'email' => 'required|string|email|max:191|ends_with:rsudcibinong.com|unique:users',
        ]);

        $userId = User::insertGetId([
            'nickname' => $request['nickname'],
            'fullname' => $request['fullname'],
            'nip' => $request['nip'],
            'email' => $request['email'],
            'role' => $request['role'],
            'jenis_peg' => $request['jenis_peg'],
            'tmt' => $request['tmt'],
            'tmt_gol' => $request['tmt_gol'],
            'gol' => $request['gol'],
            'password' => Hash::make($request['password']),
        ]);

        foreach ($request->academic as $academic) {
            if ($academic['tingkat'] != null) {
                Academic::create([
                    'id_user' => $userId,
                    'tingkat' => $academic['tingkat'],
                    'jurusan' => $academic['jurusan'],
                    'nama' => $academic['nama'],
                    'tahun' => $academic['tahun'],
                ]);
            }
        }

        Biodata::create([
            'id_user' => $userId,
            'nik' => $request->biodata['nik'],
            'agama' => $request->biodata['agama'],
            'tpt_lahir' => $request->biodata['tpt_lahir'],
            'tgl_lahir' => $request->biodata['tgl_lahir'],
            'gender' => $request->biodata['gender'],
            'hp' => $request->biodata['hp'],
        ]);

        foreach ($request->placement as $placement) {
            Placement::create([
                'id_user' => $userId,
                'id_unit' => $placement['id_unit'],
                'id_position' => $placement['id_position'],
                'id_permenpan' => $placement['id_permenpan'],
            ]);
        }

        $this->createAddress($request->alamat, $userId);

        $newUser = User::where('id', '=', $userId)
                    ->select('id', 'fullname', 'nip', 'tmt')
                    ->with('penempatan')
                    ->first();
        return ['message' => "success", 'newUser'=> $newUser];
    }

    public function createAddress($alamat, $userId)
    {
        $status = [];
        foreach($alamat as $key => $value){
            if(!empty($value)){
                array_push($status, $value);
            };
        }

        if(!empty($status)){
            Address::create([
                'id_user' => $userId,
                'jl' => $alamat['jl'],
                'kab' => $alamat['kab'],
                'kec' => $alamat['kec'],
                'kel' => $alamat['kel'],
                'kota' => $alamat['kota'],
                'kp' => $alamat['kp'],
                'no' => $alamat['no'],
                'pos' => $alamat['pos'],
                'prov' => $alamat['prov'],
                'rt' => $alamat['rt'],
                'rw' => $alamat['rw'],
            ]);
            return ['message' => "success"];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
        $query = User::where('id', '=', $user->id)
                ->where('active', '=', '1')
                ->with('placement', 'address', 'academic', 'biodata')
                ->first();
        return $query;
    }

    public function countPNS()
    {
        //
        $query = User::where('jenis_peg', '=', 'PNS')->count();
        return $query;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'nip' => 'required|unique:users,nip,'.$user->id,
            'email' => 'required|string|email|max:191|ends_with:rsudcibinong.com|unique:users,email,'.$user->id,
        ]);

        $user->update($request->except('password', 'photo'));

        if ($request->jenis_peg !== 'PNS' && $request->jenis_peg !== 'PPPK') {
            $user->update([
                'gol' => null,
                'tmt_gol' => null,
            ]);
        }

        if($request->has('placement')){
            for($i = 0; $i < count($request->placement); $i++ ){
                if($request->placement[$i]['id'] == null){
                    Placement::create([
                        'id_user' => $request->placement[$i]['id_user'],
                        'id_unit' => $request->placement[$i]['id_unit'],
                        'id_position' => $request->placement[$i]['id_position'],
                        'id_permenpan' => $request->placement[$i]['id_permenpan'],
                    ]);
                }else{
                    $row = Placement::findOrFail($request->placement[$i]['id']);
                    $row->update([
                        'id_user' => $request->placement[$i]['id_user'],
                        'id_unit' => $request->placement[$i]['id_unit'],
                        'id_position' => $request->placement[$i]['id_position'],
                        'id_permenpan' => $request->placement[$i]['id_permenpan'],
                    ]);
                }
            }
        }
        if($request->photo !== null ){
            $this->updatePhoto($request);
        }

        $updateUser = User::where('id', '=', $user->id)->where('active', '=', 1)
                        ->select('id', 'fullname', 'nickname', 'email', 'nip', 'tmt', 'photo')
                        ->with('placement', 'address', 'academic')
                        ->first();

        return ['message' => "success", 'updateUser' => $updateUser ];
    }

    public function userLapbul(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $user = User::findOrFail($request->id);

        $user->update([
            'lapbul' => $request->lapbul,
        ]);

        return ['message' => "success", 'result' => [ 'id' => $user->id, 'lapbul' => $user->lapbul ] ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return ['message' => 'Delete Success'];
    }

    public function changePassword(Request $request){

        $user = User::findOrFail($request->id);

        $this->validate($request, [
            'oldpassword' => ['required', new MatchOldPassword],
            'newpassword' => 'required|min:6|different:oldpassword',
            'password_confirmation' => 'required|same:newpassword'
        ]);

        if (Hash::check($request->oldpassword, $user->password)) {
            $user->fill([
                'password' => Hash::make($request->newpassword)
            ])->save();
            return ['message' => "change password success"];
        } else {
            return false;
        }
    }

    public function resetPassword(Request $request){
        $this->validate($request, [
            'password' => 'sometimes|min:6|confirmed',
        ]);

        $user = User::where('id', '=', $request->id)->first();

        $user->update(['password' => bcrypt($request->input('password'))]);
        return ['message' => "success"];
    }

    public function updatePhoto(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required',
        ]);

        $user = User::findOrFail($request->id);

        $currentPhoto = $user->photo;
        if($request->photo != $currentPhoto){
            $name = trim($request->fullname, ' ').'-'.time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

            \Image::make($request->photo)->save(public_path('images/avatar/').$name);
            $request->merge(['photo' => $name]);

            $userAvatar = public_path('images/avatar/').$currentPhoto;
            if(file_exists($userAvatar)){
                @unlink($userAvatar);
            }
        }
        $user->update($request->all());
        return ['message' => "success"];
    }
}
