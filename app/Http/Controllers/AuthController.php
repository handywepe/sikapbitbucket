<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Providers\RouteServiceProvider;
use Illuminate\Session\SessionManager;

class AuthController extends Controller
{
    //
    public function index()
    {
        if(Auth::check()){
            return redirect(RouteServiceProvider::HOME);
        }else{
            session()->put('url.intended', url()->previous());
            return view('application');
        }
    }
}
