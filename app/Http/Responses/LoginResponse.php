<?php

namespace App\Http\Responses;

use Illuminate\Support\Facades\Auth;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;
use Illuminate\Support\Facades\Log;
use App\Models\PersonalToken;



class LoginResponse implements LoginResponseContract
{

    public function toResponse($request)
    {

        // below is the existing response
        // replace this with your own code
        // the user can be located with Auth facade

        // $home = Auth::user()->is_admin ? config('fortify.dashboard') : config('fortify.home');

        // return $request->wantsJson()
        //             ? response()->json(['two_factor' => false])
        //             // ? $user->createToken($request->email)->plainTextToken
        //             : redirect()->intended(config('fortify.home'));

        $user = Auth::user();
        $token = PersonalToken::where('name', '=', $user->nip)->first();

        if (empty($token)) {
            return $user->createToken($user->nip)->plainTextToken;
        }


    }

}
