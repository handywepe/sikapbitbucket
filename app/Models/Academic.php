<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Academic extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_user', 'tingkat', 'jurusan', 'nama', 'tahun'
    ];

    public function user(){
        return $this->hasOne(User::class, 'id_user');
    }
}
