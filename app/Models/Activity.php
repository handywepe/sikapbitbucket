<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;
    protected $table = "activities";

    protected $fillable = [
        'id_user', 'id_skp', 'deskripsi', 'lampiran', 'biaya', 'mutu', 'jam', 'tanggal', 'status', 'mr', 'nama_pasien'
    ];

    public function skp(){
        return $this->belongsTo(Skp::class, 'id_skp');
    }
}
