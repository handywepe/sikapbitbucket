<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $fillable = [
        'id', 'id_user', 'kp', 'jl', 'rt', 'rw', 'no', 'kel', 'kec', 'kota', 'kota', 'kab', 'prov', 'pos'
    ];

    public function users(){
        return $this->belongsTo(User::class);
    }

    public function disableuser(){
        return $this->belongsTo(DisableUser::class);
    }
}
