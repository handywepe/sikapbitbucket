<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DisableUser extends Model
{
    use HasFactory;

    protected $table = 'disable_users';

    protected $fillable = [
        'id', 'id_user', 'tmt_keluar', 'alasan_keluar',
    ];

    public function jabpns()
    {
        return $this->belongsTo(Position::class, 'id_jabpns');
    }

    public function jafung()
    {
        return $this->belongsTo(Position::class, 'id_jafung');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'id_unit');
    }
    public function address()
    {
        return $this->hasOne(Address::class, 'id_user');
    }

    public function academic()
    {
        return $this->hasOne(Academic::class, 'id_user');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}
