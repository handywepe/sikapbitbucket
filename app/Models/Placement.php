<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class Placement extends Model
{
    use HasFactory, HasApiTokens;

    protected $fillable = [
        'id_user', 'id_position', 'id_unit', 'id_permenpan'
    ];

    public $timestamps = false;

    public function position(){
        return $this->belongsTo(Position::class, 'id_position');
    }
    public function unit(){
        return $this->belongsTo(Unit::class, 'id_unit');
    }
    public function user(){
        return $this->belongsTo(User::class, 'id_user');
    }


}
