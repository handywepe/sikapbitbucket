<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    use HasFactory;

    protected $fillable = [
        'jabatan', 'kategori', 'shifting', 'jenis_jabatan', 'level'
    ];

    public function disableusers()
    {
        return $this->hasOne(DisableUser::class);
    }

    public function users(){
        return $this->hasOne(User::class);
    }
}
