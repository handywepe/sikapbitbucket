<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;

    protected $table = 'mreports';

    protected $fillable = [
        'id_user', 'plan', 'do', 'check', 'action', 'date', 'status', 'catatan', 'checkdate', 'created_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function file()
    {
        return $this->hasMany(File::class, 'id_report');
    }


}
