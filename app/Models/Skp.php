<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Skp extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_user', 'id_tupoksi', 't_qty', 't_waktu', 't_biaya', 'r_qty', 'r_mutu', 'r_waktu', 'r_biaya', 'penghitungan', 'capaian'
    ];

    public function tupoksi(){
        return $this->belongsTo(Tupoksi::class, 'id_tupoksi');
    }

    public function user(){
        return $this->belongsTo(User::class, 'id_user');
    }
}
