<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tupoksi extends Model
{
    use HasFactory;

    protected $table = 'tupoksis';

    protected $fillable = [
        'id_jabatan', 'butir_kegiatan', 'angka_kredit', 'satuan_hasil', 'qty'
    ];

    public function jabatan(){
        return $this->belongsTo(Position::class, 'id_jabatan');
    }

}
