<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnitStructures extends Model
{
    use HasFactory;

    protected $table = 'unit_structures';

    public function unit(){
        return $this->belongsTo(Unit::class, 'id_unit');
    }
}
