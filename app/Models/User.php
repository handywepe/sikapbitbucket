<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nickname', 'fullname', 'nip', 'nik', 'gol', 'email', 'tmt_gol', 'tmt', 'id_alam', 'lapbul',
        'id_jafung', 'id_jabpns', 'id_edu', 'id_unit', 'USERID', 'jenis_peg', 'agama',
        'tpt_lahir', 'tgl_lahir', 'gender', 'password', 'photo', 'hp',
        'role', 'bio', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $appends = [
        'capaian',
        // 'avgBehavior',
        'prestasi',
    ];

    // protected $with = [
    //     'skp',
    //     'behavior',
    // ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'id_unit');
    }

    public function address()
    {
        return $this->hasOne(Address::class, 'id_user');
    }

    public function academic()
    {
        return $this->hasMany(Academic::class, 'id_user')->orderBy('tahun', 'desc');
    }

    public function disable(){
        return $this->hasOne(DisableUser::class, 'id_user');
    }

    public function placement(){
        return $this->hasMany(Placement::class, 'id_user')
            ->join('positions', 'positions.id', '=', 'placements.id_position')
            ->join('units', 'units.id', '=', 'placements.id_unit')
            ->select('placements.*', 'positions.level', 'units.nama_unit', 'units.bagian', 'positions.jabatan', 'positions.kategori AS katjab', 'positions.jenis_jabatan AS jenisjab', 'positions.shifting', 'units.shifting_unit')
            ->orderBy('level', 'desc');
    }
    public function penempatan(){
        return $this->hasMany(Placement::class, 'id_user')
            ->join('positions', 'positions.id', '=', 'placements.id_position')
            ->join('units', 'units.id', '=', 'placements.id_unit')
            ->select('placements.*', 'positions.level', 'units.nama_unit', 'positions.jabatan', 'positions.jenis_jabatan AS jenisjab',)
            ->orderBy('level', 'desc');
    }

    public function biodata(){
        return $this->hasOne(Biodata::class, 'id_user');
    }

    public function accept(){
        return $this->hasMany(Activity::class, 'id_user')->where('status', '=', 'Disetujui');
    }

    public function denied(){
        return $this->hasMany(Activity::class, 'id_user')->where('status', '=', 'Ditolak');
    }

    public function wait(){
        return $this->hasMany(Activity::class, 'id_user')->where('status', '=', 'Menunggu');
    }

    public function skp(){
        return $this->hasMany(Skp::class, 'id_user');
    }

    public function behavior(){
        return $this->hasOne(Behavior::class, 'id_user');
    }

    public function getCapaianAttribute(){
        return round($this->skp->avg('capaian'),2);
    }

    public function getAvgBehaviorAttribute(){
        return $this->behavior()->pluck('average')->first();
    }

    public function getPrestasiAttribute(){
        $avgBehavior = $this->getAvgBehaviorAttribute();
        $capaian = $this->getCapaianAttribute();
        return round(($capaian * 60/100) + ($avgBehavior * 40/100), 2);
    }

}
