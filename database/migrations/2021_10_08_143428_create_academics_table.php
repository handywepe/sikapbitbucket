<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcademicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_user')->nullable();
            $table->enum('tingkat',
                ['SD', 'SMP', 'SMU', 'SMA', 'SMEA', 'MA', 'SMK', 'STM', 'SMF', 'D1', 'D2', 'D3', 'D4', 'S1', 'S2', 'S3']
            );
            $table->string('jurusan');
            $table->string('nama');
            $table->year('tahun');
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academics');
    }
}
