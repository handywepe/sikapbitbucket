export default class Gate{

    constructor(user){
        this.user = user;
    }

    isLapbul(){
        return this.user.lapbul == 1;
    }

    isSysDev(){
        return this.user.role === 'administrator';
    }

    isExecutive(){
        return this.user.role === 'executive';
    }

    isSuperAdminPlus(){
        return this.user.role === 'super-admin-plus';
    }

    isSuperAdmin(){
        return this.user.role === 'super-admin';
    }

    isAdmin(){
        return this.user.role === 'admin';
    }

    isUser(){
        return this.user.role === 'user';
    }

    isUP(){
        const placement = this.user.placement;
        let x = placement.filter(p => p.id_unit === 75 );
        if (x.length !== 0) {
            return true;
        }
    }

    isKeuangan(){
        if(this.user.id_unit === 5 ){
            return true;
        }
    }
    // 2 user
    isEXEandSYS(){
        if(this.user.role === 'executive' || this.user.role === 'sysdev'){
            return true;
        }
    }
    isSAPandSYS(){
        if(this.user.role === 'super-admin-plus' || this.user.role === 'sysdev'){
            return true;
        }
    }
    isSAandSYS(){
        if(this.user.role === 'super-admin' || this.user.role === 'sysdev'){
            return true;
        }
    }
    isAandSYS(){
        if(this.user.role === 'admin' || this.user.role === 'sysdev'){
            return true;
        }
    }
    isUandSYS(){
        if(this.user.role === 'user' || this.user.role === 'sysdev'){
            return true;
        }
    }

    isAandSAandSYS(){
        if(this.user.role === 'admin' || this.user.role === 'super-admin' || this.user.role === 'sysdev'){
            return true;
        }
    }
    isSAandEXECandSYS(){
        if(this.user.role === 'executive' || this.user.role === 'super-admin' || this.user.role === 'sysdev'){
            return true;
        }
    }
    isAandSAandEXECandSYS(){
        if(this.user.role === 'admin' || this.user.role === 'super-admin' || this.user.role === 'sysdev' || this.user.role === 'executive' ){
            return true;
        }
    }

    isIT(){
        const jabatan = user.placement;
        let x = jabatan.filter(jab => jab.id_position === 24 || jab.id_position === 2 );
        if (x.length !== 0) {
            return true;
        }
    }

//new Level
    isDir(){
        const placement = this.user.placement;
        let x = placement.filter(p => p.level === 8 );
        if (x.length !== 0) {
            return true;
        }
    }
    isWadir(){
        const placement = this.user.placement;
        let x = placement.filter(p => p.level === 7 );
        if (x.length !== 0) {
            return true;
        }
    }
    isKaBag(){
        const placement = this.user.placement;
        let x = placement.filter(p => p.level === 6.5 );
        if (x.length !== 0) {
            return true;
        }
    }

    isKaSubBag(){
        const placement = this.user.placement;
        let x = placement.filter(p => p.level === 5.5 );
        if (x.length !== 0) {
            return true;
        }
    }
    isAgtKaSubBag(){
        const placement = this.user.placement;;
        let x = placement.filter(p => p.level === 5.0 );
        if (x.length !== 0) {
            return true;
        }
    }
    isKaKomite(){
        const placement = this.user.placement;;
        let x = placement.filter(p => p.level === 4.5 );
        if (x.length !== 0) {
            return true;
        }
    }
    isAgtKaKomite(){
        const placement = this.user.placement;;
        let x = placement.filter(p => p.level === 4.0 );
        if (x.length !== 0) {
            return true;
        }
    }
    isKaIns(){
        const placement = this.user.placement;;
        let x = placement.filter(p => p.level === 3.5 );
        if (x.length !== 0) {
            return true;
        }
    }
    isKaru(){
        const placement = this.user.placement;;
        let x = placement.filter(p => p.level === 2.5 );
        if (x.length !== 0) {
            return true;
        }
    }
    // || p.level === 4 || p.level === 5 || p.level === 6
    isHighStaff(){
        const placement = this.user.placement;;
        let x = placement.filter(p => p.level === 4 || p.level === 5 || p.level === 6);
        if (x.length !== 0) {
            return true;
        }
    }
    isStaff(){
        const placement = this.user.placement;;
        let x = placement.filter(p => p.level === 1 || p.level === 2 || p.level === 3);
        if (x.length !== 0) {
            return true;
        }
    }

    isNakes(){
        const placement = this.user.placement;;
        let x = placement.filter(p => p.katjab === 'Tenaga Kesehatan');
        if (x.length !== 0) {
            return true;
        }
    }

    isShift(){
        const placement = this.user.placement;;
        let x = placement.filter(p => p.shifting === 'shift');
        if (x.length !== 0) {
            return true;
        }
    }
    isShiftUnit(){
        const placement = this.user.placement;;
        let x = placement.filter(p => p.shifting_unit === 'shift');
        if (x.length !== 0) {
            return true;
        }
    }

    isReadOnly(){
        const placement = this.user.placement;;
        let x = placement.filter(p => p.level === 1 || p.level === 2 || p.level === 3 || p.level === 3.5 || p.level === 4 || p.level === 4.5 );
        if (x.length !== 0) {
            return true;
        }
    }

}
