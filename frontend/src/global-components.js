import Vue from 'vue'
import FeatherIcon from '@core/components/feather-icon/FeatherIcon.vue'
Vue.component(FeatherIcon.name, FeatherIcon)

import Api from "@axios"
Vue.use(Api)

//BootstrapsVue
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { BFormFile } from 'bootstrap-vue'
import { CardPlugin } from 'bootstrap-vue'
import { VBScrollspyPlugin } from 'bootstrap-vue'
import { DropdownPlugin, TablePlugin } from 'bootstrap-vue'

// BootstrapVue Register
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(CardPlugin)
Vue.use(VBScrollspyPlugin)
Vue.use(DropdownPlugin)
Vue.use(TablePlugin)
Vue.component('b-form-file', BFormFile)

//vee validate
import { ValidationProvider, ValidationObserver } from 'vee-validate'
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
import { required, email, min, image, max} from '@validations';
import { extend } from 'vee-validate';
extend('required', {
    ...required,
    message: 'Kolom {_field_} harus diisi'
});
extend('image', {
    ...image,
    message: 'File harus berformat gambar (.jpg, .png, .jpeg)'
});
extend('between', {
    ...between,
    message: 'nilai minimal {min} hingga {max}'
});

//vform validation
import {Form} from 'vform';
window.Form = Form;
import { HasError, AlertError } from 'vform/src/components/bootstrap4'
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

//currency filter
import VueCurrencyFilter from 'vue-currency-filter'
Vue.use(VueCurrencyFilter,
    {
      symbol : 'Rp.',
      thousandsSeparator: '.',
      fractionCount: 2,
      fractionSeparator: ',',
      symbolPosition: 'front',
      symbolSpacing: true,
      avoidEmptyDecimals: undefined,
    })

import SmartTable from 'vuejs-smart-table'
import { between } from 'vee-validate/dist/rules'
Vue.use(SmartTable);

require('@fortawesome/fontawesome-free/js/fontawesome');
require('@fortawesome/fontawesome-free/js/brands');
require('@fortawesome/fontawesome-free/js/solid');



