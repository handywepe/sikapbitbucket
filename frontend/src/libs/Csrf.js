import Api from "./axios";

export default {
    getCookie(){
       return Api.get("/sanctum/csrf-cookie");
    }
}