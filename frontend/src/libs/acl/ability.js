import { Ability } from '@casl/ability'
import { initialAbility } from './config'
import Gate from '@/Gate'

const userData = JSON.parse(localStorage.getItem('userLogin'))
const Guard = new Gate(userData);

var permission = [{ action: 'read', subject: 'basic'}]
if (userData) {

    permission.push({ action: 'read', subject: 'auth' })

    if(Guard.isSysDev()) {
        permission.push({ action: 'manage', subject: 'all' })
    }

    if(Guard.isUP()) {
        permission.push({ action: 'manage', subject: 'pegawai' })
    }

    if(Guard.isDir()) {
        permission.push(
            { action: 'read', subject: 'pegawai' },
            { action: 'read', subject: 'pejabat' },
            { action: 'read', subject: 'direktur' },
        )
    }

    if(Guard.isWadir()) {
        permission.push(
            { action: 'read', subject: 'pegawai' },
            { action: 'read', subject: 'pejabat' },
        )
    }

    if(Guard.isKaBag()) {
        permission.push(
            { action: 'read', subject: 'pegawai' },
            { action: 'read', subject: 'pejabat' },
        )
    }

    if(Guard.isKaSubBag()) {
        permission.push(
            { action: 'read', subject: 'pegawai' },
            { action: 'read', subject: 'pejabat' },
        )
    }

    if(Guard.isAgtKaSubBag()) {
        permission.push(
            { action: 'read', subject: 'pegawai' },
            // { action: 'read', subject: 'pejabat' },
        )
    }

    if(Guard.isKaIns()) {
        permission.push(
            { action: 'read', subject: 'pegawai' },
            { action: 'read', subject: 'pejabat' },
        )
    }

    if(Guard.isKaru()) {
        permission.push(
            { action: 'read', subject: 'pegawai' },
            { action: 'read', subject: 'pejabat' },
        )
    }

    if(Guard.isLapbul()) {
        permission.push(
            { action: 'read', subject: 'lapbul' },
        )
    }
}

const result = permission
localStorage.setItem('permission', JSON.stringify(result));
export default new Ability(result)
