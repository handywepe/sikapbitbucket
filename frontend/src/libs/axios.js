import Vue from 'vue'

// axios
import axios from 'axios'

const Api = axios.create({
    // You can add your headers here
    // ================================
      baseURL: 'http://sikap-v2.com/api',
    //   baseURL: 'http://127.0.0.1:8000/api',
    // baseURL: 'http://10.122.131.57:8080/api',
    // timeout: 1000,
    // headers: {'X-Custom-Header': 'foobar'}
})

// let token = localStorage.getItem("token");
// Api.defaults.headers.common = {'Authorization': `bearer ${token}`}

// Api.defaults.withCredentials = true;

Vue.prototype.$http = Api

export default Api;
