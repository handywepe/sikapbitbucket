import Vue from 'vue'
window.Fire = new Vue();
window.Vue = require('vue');

import router from './router'
import store from './store'
import App from './App.vue'

//import BSV
import { ToastPlugin, ModalPlugin, } from 'bootstrap-vue'

import VueCompositionAPI from '@vue/composition-api'

// 3rd party plugins
import '@/libs/portal-vue'
import '@/libs/toastification'
import '@/libs/sweet-alerts'


import Gate from "./Gate";
Vue.prototype.$gate = new Gate(window.user);

// Global Components
import './global-components'
import moment from 'moment'
import 'moment/locale/id';
import '@/libs/acl'

require('@core/assets/fonts/feather/iconfont.css')

//print
import VueHtmlToPaper from 'vue-html-to-paper';
Vue.use(VueHtmlToPaper);
import '@/views/skp/print.css'

//filter text and date
Vue.filter('uptext', function(text){
    return text.charAt(0).toUpperCase() + text.slice(1)
});

Vue.filter('jamAct', function(text){
    return text.substring(0, 5);
});

Vue.filter('bulanTahun', function(created){
    return moment(created).format('MMMM YYYY');
});

Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('Do MMMM YYYY')
    }
});

Vue.filter('tanggal', function(created){
    return moment(created).format('dddd, Do MMMM YYYY');
});

Vue.filter('truefalse', function(text){
    if(text == 1){
        return 'Ya';
    }else{
        return 'Tidak';
    }
});


// BSV Plugin Registration
Vue.use(ToastPlugin)
Vue.use(ModalPlugin)

// Composition API
Vue.use(VueCompositionAPI)

// import core styles
require('@core/scss/core.scss')

// import assets styles
require('@/assets/scss/style.scss')

Vue.config.productionTip = false

//asset path
Vue.mixin(require('./path'));

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'

Vue.use(Vuetify)

export default new Vuetify({
    breakpoint: {
        thresholds: {
            xs: 0,
            sm: 576,
            md: 768,
            lg: 992,
            xl: 1200,
        }
    }
})

const vuetify = new Vuetify({
    theme: {
        // dark: false,
        themes: {
            light: {
                primary: '#39af21',
            //   secondary: '#b0bec5',
                accent: '#39af21',
            //   error: '#b71c1c',
            },
            dark: {
                primary: '#39af21',
                secondary: '#82868b',
                accent: '#39af21',
            //   error: '#b71c1c',
            },
        },
    },
})

new Vue({
    vuetify,
    router,
    store,
    data() {
        return {
            ready : false,
        }
    },
    mounted() {
        this.vuetifyTheme();
        this.$store.dispatch("user/getUserLogin")
        .then(() => { this.ready = true; })
        .catch(() => { this.ready = true })
    },
    methods: {
        vuetifyTheme(){
            this.$vuetify.theme.dark = this.isDark;
        }
    },
    computed:{
        isDark(){
            return this.$store.state.appConfig.layout.isDark;
        },
        userLogin(){
            return this.$store.state.user.userLogin;
        }
    },
    watch:{
        isDark : 'vuetifyTheme'
    },
    render: h => h(App),
}).$mount('#app')
