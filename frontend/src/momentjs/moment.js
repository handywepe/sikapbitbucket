import Vue from 'vue'

import moment from "moment";
import 'moment/locale/id';

Vue.filter('masabakti', function(tmt){
    return moment(tmt).fromNow(true);
});
Vue.filter('tanggal', function(created){
    return moment(created).format('dddd, Do MMMM YYYY');
});
Vue.filter('toDays', function(created){
    var a = moment(new Date());
    var b = moment(created);
    return a.diff(b, 'days');
});