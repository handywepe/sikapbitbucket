export default [
    {
        title: 'Dashboard',
        route: 'dashboard',
        icon: 'HomeIcon',
        resource: 'auth',
        action: 'read',
    },
    {
        header: 'Pegawai',
        icon: 'UsersIcon',
        children: [
            {
                title: 'Pegawai',
                route: 'pegawai',
                icon: 'UsersIcon',
                resource: 'pegawai',
                action: 'read',
            },
            {
                title: 'Pegawai Non-Aktif',
                route: 'pegawai-nonaktif',
                icon: 'UserXIcon',
                resource: 'pegawai',
                action: 'read',
            },
        ],
    },
    {
        header: 'Master Data',
        icon: 'DatabaseIcon',
        children: [
            {
                title: 'Unit',
                route: 'units',
                icon: 'BoxIcon',
                resource: 'admin',
                action: 'read',
            },
            {
                title: 'Jabatan',
                route: 'positions',
                icon: 'BriefcaseIcon',
                resource: 'admin',
                action: 'read',
            },
        ],
    },
]
