import Vue from 'vue'
import VueRouter from 'vue-router'
import ability from '../libs/acl/ability'

//web
import pegawai from './web/pegawai'
import masterdata from './web/masterdata'
import skp from './web/skp'
import laporanBulanan from './web/laporanBulanan'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior(to, from, next) {
        return { x: 0, y: 0 }
    },
    routes: [
        { path: '/', redirect: { name: 'dashboard' } },
        {   path: '/dashboard',
            name: 'dashboard',
            component: () => import('@/views/Dashboard.vue'),
            meta: {
                pageTitle: 'Dashboard',
                breadcrumb: [
                    { text: 'Dashboard', active: true, },
                ],
                resource: 'auth',
                action: 'read',
            }
        },
        {   path: '/login',
            name: 'login',
            component: () => import('@/views/Login.vue'),
            meta: {
                layout: 'full',
                resource: 'basic',
                action: 'read'
            },
        },
        {   path: '/error-404',
            name: 'error-404',
            component: () => import('@/views/error/Error404.vue'),
            meta: {
                layout: 'full',
                resource: 'basic',
                action: 'read',
            },
        },
        {   path: '/not-authorized',
            name: 'not-authorized',
            component: () => import('@/views/error/NotAuthorized.vue'),
            meta: {
                layout: 'full',
                resource: 'basic',
                action: 'read',
            },
        },
        {   path: '/setting',
            name: 'setting',
            component: () => import('@/views/account-setting/AccountSetting.vue'),
            meta: {
                pageTitle: 'Pengaturan Akun',
                breadcrumb: [
                    { text: 'Pengaturan Akun', active: true, },
                ],
                resource: 'auth',
                action: 'read',
            }
        },
        { path: '*', redirect: 'error-404', },
        ...pegawai,
        ...masterdata,
        ...skp,
        ...laporanBulanan,
    ],
})

router.beforeEach((to, from, next) => {
    const canNavigate = to.matched.some(route => {
        return ability.can(route.meta.action || 'read', route.meta.resource)
    })

    if (!canNavigate) {
        return next('/not-authorized')
    }

    next()
})

// ? For splash screen
// Remove afterEach hook if you are not using splash screen
router.afterEach(() => {
    // Remove initial loading
    const appLoading = document.getElementById('loading-bg')
    if (appLoading) {
        appLoading.style.display = 'none'
    }
})

export default router
