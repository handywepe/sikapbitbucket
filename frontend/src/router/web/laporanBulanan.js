export default [
    {   path: '/laporan-bulanan',
        name: 'report',
        component: () => import('@/views/reports/Report.vue'),
        meta: {
            pageTitle: 'Laporan Bulanan',
            breadcrumb: [
                { text: 'Laporan Bulanan', active: true, },
            ],
            resource: 'lapbul',
            action: 'read',
        }
    },
]
