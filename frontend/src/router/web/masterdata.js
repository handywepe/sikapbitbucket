export default [
    //unit
    {   path: '/units',
        name: 'units',
        component: () => import('@/views/master-data/unit/Units.vue'),
        meta: {
            pageTitle: 'Unit',
            breadcrumb: [
                { text: 'Master Data', active: true, },
                { text: 'Unit', active: true, },
            ],
            resource: 'admin',
            action: 'read',
        }
    },
    //jabatan
    {   path: '/positions',
        name: 'positions',
        component: () => import('@/views/master-data/position/Positions.vue'),
        meta: {
            pageTitle: 'Jabatan',
            breadcrumb: [
                { text: 'Master Data', active: true, },
                { text: 'Jabatan', active: true, },
            ],
            resource: 'admin',
            action: 'read',
        }
    },
    //user lapbul
    {   path: '/user-lapbul',
        name: 'userlapbul',
        component: () => import('@/views/master-data/userlapbul/UserLapbul.vue'),
        meta: {
            pageTitle: 'User Laporan Bulanan',
            breadcrumb: [
                { text: 'Master Data', active: true, },
                { text: 'User Laporan Bulanan', active: true, },
            ],
            resource: 'admin',
            action: 'read',
        }
    },
    {   path: '/tupoksi-jabatan',
        name: 'tupoksi',
        component: () => import('@/views/master-data/tupoksi/Tupoksi.vue'),
        meta: {
            pageTitle: 'Tupoksi Jabatan',
            breadcrumb: [
                { text: 'Master Data', active: true, },
                { text: 'Tupoksi Jabatan', active: true, },
            ],
            resource: 'pegawai',
            action: 'manage',
        }
    },
]
