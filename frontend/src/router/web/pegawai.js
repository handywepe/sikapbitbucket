export default [
    //pegawai-list
    {   path: '/pegawai',
        name: 'pegawai',
        component: () => import('@/views/pegawai/PegawaiList.vue'),
        meta: {
            pageTitle: 'Pegawai',
            breadcrumb: [
                { text: 'Pegawai', active: true, },
            ],
            resource: 'pegawai',
            action: 'read',
        },
    },
    //pegawai-view
    {   path: '/pegawai/view/:id',
        name: 'pegawai-view',
        component: () => import('@/views/pegawai/view-pegawai/PegawaiView.vue'),
        meta: {
            pageTitle: 'Profil Pegawai',
            breadcrumb: [
                { text: 'Pegawai', to: '/pegawai' },
                { text: 'Profil Pegawai', active: true, },
            ],
            resource: 'pegawai',
            action: 'read',
        }
    },
    //tambah-pegawai
    {   path: '/pegawai/add',
        name: 'pegawai-add',
        component: () => import('@/views/pegawai/add-pegawai/TambahPegawai.vue'),
        meta: {
            pageTitle: 'Tambah Pegawai',
            breadcrumb: [
                { text: 'Pegawai', to: '/pegawai' },
                { text: 'Tambah Pegawai', active: true, },
            ],
            resource: 'pegawai',
            action: 'manage',
        }
    },
    //pegawai-edit
    {   path: '/pegawai/edit/:id',
        name: 'pegawai-edit',
        component: () => import('@/views/pegawai/edit-pegawai/EditPegawai.vue'),
        meta: {
            pageTitle: 'Edit Data Pegawai',
            breadcrumb: [
                { text: 'Pegawai', to: '/pegawai' },
                { text: 'Edit Pegawai', active: true, },
            ],
            resource: 'pegawai',
            action: 'manage',
        }
    },
    //pegawai-non-aktif
    {   path: '/pegawai-nonaktif',
        name: 'pegawai-nonaktif',
        component: () => import('@/views/pegawai/PegawaiNonAktif.vue'),
        meta: {
            pageTitle: 'Pegawai Non Aktif',
            breadcrumb: [
                { text: 'Pegawai Non Aktif', active: true, },
            ],
            resource: 'pegawai',
            action: 'manage',
        },
    },
    //pegawai-non-aktif
    {   path: '/profil',
        name: 'profil',
        component: () => import('@/views/pegawai/ProfilPegawai.vue'),
        meta: {
            pageTitle: 'Profil',
            breadcrumb: [
                { text: 'Profil', active: true, },
            ],
            resource: 'auth',
            action: 'read',
        },
    },
]
