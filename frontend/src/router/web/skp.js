export default [
    //skp
    {   path: '/skp-tahunan',
        name: 'skp',
        component: () => import('@/views/skp/SkpTahunan.vue'),
        meta: {
            pageTitle: 'SKP',
            breadcrumb: [
                { text: 'SKP', active: true, },
                { text: 'SKP Tahunan', active: true, },
            ],
            resource: 'auth',
            action: 'read',
        }
    },
    {   path: '/laporan-skp',
        name: 'laporanSkp',
        component: () => import('@/views/skp/LaporanSkp.vue'),
        meta: {
            pageTitle: 'Laporan SKP',
            breadcrumb: [
                { text: 'SKP', active: true, },
                { text: 'Laporan SKP', active: true, },
            ],
            resource: 'auth',
            action: 'read',
        }
    },
    {   path: '/skp-pegawai',
        name: 'skpPegawai',
        component: () => import('@/views/skp/SkpPegawai.vue'),
        meta: {
            pageTitle: 'SKP Pegawai',
            breadcrumb: [
                // { text: 'Penilaian', active: true, },
                { text: 'SKP Pegawai', active: true, },
            ],
            resource: 'pegawai',
            action: 'read',
        }
    },
    {   path: '/skp-activity',
        name: 'activity',
        component: () => import('@/views/activity/Activity.vue'),
        meta: {
            pageTitle: 'Kegiatan',
            breadcrumb: [
                { text: 'SKP', active: true, },
                { text: 'Kegiatan', active: true, },
            ],
            resource: 'auth',
            action: 'read',
        }
    },
    {   path: '/activity-assesment',
        name: 'assesment',
        component: () => import('@/views/assesment/Assesment.vue'),
        meta: {
            pageTitle: 'Penilaian Kegiatan',
            breadcrumb: [
                { text: 'SKP', active: true, },
                { text: 'Penilaian Kegiatan', active: true, },
            ],
            resource: 'pejabat',
            action: 'read',
        },
        children : [{
            path: '/activity-assesment/:name/:id_user/:date',
            name: 'assesmentlist',
            component: () => import('@/views/assesment/Timeline.vue'),
            props: { default: true },
            meta: {
                pageTitle: 'Penilaian Kegiatan',
                breadcrumb: [
                    { text: 'SKP', active: true, },
                    { text: 'Penilaian Kegiatan', active: true, },
                ],
                resource: 'pejabat',
                action: 'read',
            },
        }]
    },
    {   path: '/penilaian-perilaku',
        name: 'behavior',
        component: () => import('@/views/behavior/Behavior.vue'),
        meta: {
            pageTitle: 'Penilaian Perilaku',
            breadcrumb: [
                // { text: 'SKP', active: true, },
                { text: 'Penilaian Perilaku', active: true, },
            ],
            resource: 'pejabat',
            action: 'read',
        },
    },
    {   path: '/prestasi-pegawai',
        name: 'prestasiPegawai',
        component: () => import('@/views/skp/PrestasiPegawai.vue'),
        meta: {
            pageTitle: 'Prestasi Pegawai',
            breadcrumb: [
                // { text: 'SKP', active: true, },
                { text: 'Prestasi Pegawai', active: true, },
            ],
            resource: 'pegawai',
            action: 'read',
        },
    },
    {   path: '/prestasiku',
        name: 'prestasiku',
        component: () => import('@/views/skp/Prestasi.vue'),
        meta: {
            pageTitle: 'Prestasiku',
            breadcrumb: [
                // { text: 'SKP', active: true, },
                { text: 'Prestasi', active: true, },
            ],
            resource: 'auth',
            action: 'read',
        },
    },
]
