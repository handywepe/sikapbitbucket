import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import app from './app'
import appConfig from './app-config'
import verticalMenu from './vertical-menu'
import user from './user'
import unit from './unit'
import position from './position'
import skp from './skp'
import rkk from './rkk'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app,
    appConfig,
    verticalMenu,
    user,
    unit,
    position,
    skp,
    rkk,
  },
  strict: process.env.DEV,
})
