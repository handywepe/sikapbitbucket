import Api from "@axios";

export default {
    namespaced: true,
    state: {
        positions: [],
    },
    getters: {

    },
    mutations: {
        SET_POSITION (state, data) {
            state.positions = data;
        },
        CREATE_POSITION (state, newPos) {
            state.positions.unshift( newPos );
        },
        UPDATE_POSITION (state, update) {
            let index = state.positions.map(e => { return e.id; }).indexOf(update.id);
            state.positions.splice(index, 1, update);
        },
        DELETE_POSITION (state, id) {
            let index = state.positions.map(e => { return e.id; }).indexOf(id);
            state.positions.splice(index, 1);
        },
    },
    actions: {
        getPosition ({ commit }) {
            return new Promise((resolve, reject) => {
                Api.get("/position")
                .then(response => {
                    resolve(response);
                    commit('SET_POSITION', response.data );
                })
                .catch(error => reject(error))
            })
        },
        createPosition({ commit }, request) {
            return new Promise((resolve, reject) => {
                Api
                .post('position', request)
                .then(response => {
                    resolve(response);
                    commit('CREATE_POSITION', response.data.newPos);
                })
                .catch(error => reject(error))
            })
        },
        updatePosition({ commit }, request) {
            return new Promise((resolve, reject) => {
                Api
                .put('position/'+request.id, request)
                .then(response => {
                    resolve(response);
                    commit('UPDATE_POSITION', response.data.updatePos);
                })
                .catch(error => reject(error))
            })
        },
        deletePosition({ commit }, id) {
            return new Promise((resolve, reject) => {
                Api
                .delete('position/'+id)
                .then(response => {
                    resolve(response);
                    commit('DELETE_POSITION', id);
                })
                .catch(error => reject(error))
            })
        },
    },
}
