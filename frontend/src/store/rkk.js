import Api from "@axios";

export default {
    namespaced: true,
    state: {
        rkks: [],
    },
    getters: {

    },
    mutations: {
        SET_RKK (state, data) {
            state.rkks = data;
        },
    },
    actions: {
        getRkk({ commit }, request) {
            return new Promise((resolve, reject) => {
                Api
                .get('rkk', request)
                .then(response => {
                    resolve(response);
                    commit('SET_RKK', response.data);
                })
                .catch(error => reject(error))
            })
        },
    },
}
