import Api from "@axios";

export default {
    namespaced: true,
    state: {
        skps: [],
        tupoksi: [],
    },
    getters: {

    },
    mutations: {
        SET_TUPOKSI (state, data) {
            state.tupoksi = data;
        },
        SET_SKP (state, data) {
            state.skps = data;
        },
        CREATE_SKP (state, newSkp) {
            state.skps.unshift( newSkp );
        },
        UPDATE_SKP (state, update) {
            let index = state.skps.map(e => { return e.id; }).indexOf(update.id);
            state.skps.splice(index, 1, update);
        },
        DELETE_SKP (state, id) {
            let index = state.skps.map(e => { return e.id; }).indexOf(id);
            state.skps.splice(index, 1);
        },
    },
    actions: {
        getTupoksi ({ commit }) {
            return new Promise((resolve, reject) => {
                Api.get("/tupoksi")
                .then(response => {
                    resolve(response);
                    commit('SET_TUPOKSI', response.data );
                })
                .catch(error => reject(error))
            })
        },
        getSkp({ commit }, request) {
            return new Promise((resolve, reject) => {
                Api
                .post('skp-pegawai', request)
                .then(response => {
                    resolve(response);
                    commit('SET_SKP', response.data);
                })
                .catch(error => reject(error))
            })
        },
        createSkp({ commit }, request) {
            return new Promise((resolve, reject) => {
                Api
                .post('skp', request)
                .then(response => {
                    resolve(response);
                    commit('CREATE_SKP', response.data.created);
                })
                .catch(error => reject(error))
            })
        },
        updateSkp({ commit }, request) {
            return new Promise((resolve, reject) => {
                Api
                .put('skp/'+request.id, request)
                .then(response => {
                    resolve(response);
                    commit('UPDATE_SKP', response.data.updated);
                })
                .catch(error => reject(error))
            })
        },
        deleteSkp({ commit }, id) {
            return new Promise((resolve, reject) => {
                Api
                .delete('skp/'+id)
                .then(response => {
                    resolve(response);
                    commit('DELETE_SKP', id);
                })
                .catch(error => reject(error))
            })
        },
    },
}
