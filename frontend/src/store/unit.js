import Api from "@axios";

export default {
    namespaced: true,
    state: {
        units: [],
    },
    getters: {

    },
    mutations: {
        SET_UNIT (state, data) {
            state.units = data;
        },
        CREATE_UNIT (state, newUnit) {
            state.units.unshift( newUnit );
        },
        UPDATE_UNIT (state, update) {
            let index = state.units.map(e => { return e.id; }).indexOf(update.id);
            state.units.splice(index, 1, update);
        },
        DELETE_UNIT (state, id) {
            let index = state.units.map(e => { return e.id; }).indexOf(id);
            state.units.splice(index, 1);
        },
    },
    actions: {
        getUnit ({ commit }) {
            return new Promise((resolve, reject) => {
                Api.get("/unit")
                .then(response => {
                    resolve(response);
                    commit('SET_UNIT', response.data );
                })
                .catch(error => reject(error))
            })
        },
        createUnit({ commit }, request) {
            return new Promise((resolve, reject) => {
                Api
                .post('unit', request)
                .then(response => {
                    resolve(response);
                    commit('CREATE_UNIT', response.data.newUnit);
                })
                .catch(error => reject(error))
            })
        },
        updateUnit({ commit }, request) {
            return new Promise((resolve, reject) => {
                Api
                .put('unit/'+request.id, request)
                .then(response => {
                    resolve(response);
                    commit('UPDATE_UNIT', response.data.updateUnit);
                })
                .catch(error => reject(error))
            })
        },
        deleteUnit({ commit }, id) {
            return new Promise((resolve, reject) => {
                Api
                .delete('unit/'+id)
                .then(response => {
                    resolve(response);
                    commit('DELETE_UNIT', id);
                })
                .catch(error => reject(error))
            })
        },
    },
}
