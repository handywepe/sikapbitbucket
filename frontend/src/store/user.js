import Api from "@axios";

export default {
    namespaced: true,
    state: {
        userLogin: null,
        pegawai: [],
        atasan: null,
        access: [],
        userAssesments: [],
        ready : null,
    },
    getters: {

    },
    mutations: {
        SET_READY (state, data){
            state.ready = data;
        },
        SET_USER (state, userlogin) {
            state.userLogin = userlogin;
        },
        SET_ATASAN (state, data) {
            state.atasan = data;
        },
        SET_ACCESS (state, data) {
            state.access = data;
        },
        SET_USER_ASSESMENT (state, data) {
            state.userAssesments = data;
        },
        // SET_TOTAL_WAIT (state, data) {
        //     var join = data;
        //     join.forEach(element => {
        //         let id_user = element.id_user;
        //         let index = state.userAssesments.findIndex((element) => element.id_user == id_user)
        //         Object.assign(state.userAssesments[index], element);
        //     });
        // },
        CHANGE_WAIT(state, value){
            let index = state.userAssesments.findIndex((element) => element.id_user == value.id_user)
            if (state.userAssesments[index].wait != 0) {
                state.userAssesments[index].wait = state.userAssesments[index].wait - 1;
            }
        },
        CHANGE_BEHAVIOR(state, value){
            if(value.message == 'created'){
                let index = state.userAssesments.findIndex((element) => element.id_user == value.newPost.id_user);
                state.userAssesments[index].behavior = state.userAssesments[index].behavior - 1;
            }
        },
        SET_PEGAWAI (state, pegawai) {
            state.pegawai = pegawai;
        },
        CREATE_PEGAWAI (state, pegawai) {
            state.pegawai.unshift( pegawai.newUser );
        },
        DISABLE_PEGAWAI (state, pegawai) {
            let index = state.pegawai.map(e => { return e.id; }).indexOf(pegawai.id);
            state.pegawai.splice(index, 1);
        },
        UPDATE_PEGAWAI (state, pegawai) {
            let index = state.pegawai.map(e => { return e.id; }).indexOf(pegawai.id);
            state.pegawai[index] = pegawai;
        },
        UPDATE_LAPBUL (state, data) {
            let index = state.pegawai.map(e => { return e.id; }).indexOf(data.id);
            state.pegawai[index].lapbul = data.lapbul;
        },
        SET_USER_LAPBUL (state, data) {
            state.userLogin.lapbul = data.lapbul;
        },
    },
    actions: {
        getReady({ commit }) {
            commit('SET_READY', true);
        },
        getAccess ({ commit }) {
            return new Promise((resolve, reject) => {
                Api.get('can-access')
                .then(response => {
                    commit('SET_ACCESS', response.data)
                    resolve(response)
                })
                .catch(error => reject(error))
            })
        },
        getUserAssesment ({ commit }, request) {
            return new Promise((resolve, reject) => {
                Api.post('user-assesment', request)
                .then(response => {
                    resolve(response);
                    commit('SET_USER_ASSESMENT', response.data)
                })
                .catch(error => reject(error))
            })
        },
        getAtasan ({ commit, state }) {
            return new Promise((resolve, reject) => {
                let user = state.userLogin;
                let request = {
                    level : user.placement[0].level,
                    id_unit : user.placement[0].id_unit,
                }
                Api.post('get-atasan', request)
                .then(response => {
                    resolve(response);
                    commit('SET_ATASAN', response.data);
                })
                .catch(error => reject(error))
            })
        },
        getUserLogin ({ commit }) {
            return new Promise((resolve, reject) => {
                Api.get('/userlogin')
                .then(response => {
                    resolve(response);
                    commit('SET_USER', response.data );
                    localStorage.setItem('userLogin', JSON.stringify(response.data));
                })
                .catch(error => reject(error))
            })
        },
        getPegawaiAll ({ commit }) {
            return new Promise((resolve, reject) => {
                Api.get('/user')
                .then(response => {
                    resolve(response);
                    commit('SET_PEGAWAI', response.data );
                })
                .catch(error => reject(error))
            })
        },
        getPegawai ({ commit }) {
            return new Promise((resolve, reject) => {
                Api.get('/user-by-structure')
                .then(response => {
                    resolve(response);
                    commit('SET_PEGAWAI', response.data );
                })
                .catch(error => reject(error))
            })
        },
        createPegawai ({ commit }, request) {
            return new Promise((resolve, reject) => {
                Api
                .post('/user', request )
                .then(response => {
                    resolve(response);
                    commit('CREATE_PEGAWAI', response.data);
                })
                .catch(error => reject(error))
            })
        },
        disablePegawai({ commit }, request) {
            return new Promise((resolve, reject) => {
                Api
                .post('disable-user', request)
                .then(response => {
                    resolve(response);
                    commit('DISABLE_PEGAWAI', request);
                })
                .catch(error => reject(error))
            })
        },
        updatePegawai({ commit, state }, request) {
            return new Promise((resolve, reject) => {
                Api
                .put('user/'+request.id, request)
                .then(response => {
                    resolve(response);
                    commit('UPDATE_PEGAWAI', response.data.updateUser);
                    if(state.userLogin.id == request.id){
                        commit('SET_USER', response.data.updateUser);
                    }
                })
                .catch(error => reject(error))
            })
        },
        userLapbul({ commit, state }, request) {
            return new Promise((resolve, reject) => {
                Api
                .post('user-lapbul', request)
                .then(response => {
                    resolve(response);
                    commit('UPDATE_LAPBUL', response.data.result);
                    if(state.userLogin.id == request.id){
                        commit('SET_USER_LAPBUL', response.data.result);
                    }
                })
                .catch(error => reject(error))
            })
        },
    },
}
