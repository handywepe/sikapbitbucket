<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Session\SessionManager;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('intended', function () {
    $intended = session()->pull('url.intended');
    return $intended;
});

Route::middleware('auth:sanctum')->get('/userlogin', function (Request $request) {
    return $request->user()->load('placement');
});
// Route::get('/userlogin', function (Request $request) {
//     return $request->user();
// });

Route::get('user/generatemail', 'API\UserController@email');
Route::group(['middleware' => 'auth:sanctum'], function() {

    //pegawai
    Route::apiResources(['user' => 'API\UserController']);
    Route::get('count-pns', 'API\UserController@countPNS');

    Route::apiResources(['biodata' => 'API\BiodataController']);
    Route::get('count-gender', 'API\BiodataController@countGender');

    Route::get('user-by-structure', 'API\UserController@userByStructure');
    // Route::get('user-assesment', 'API\UserController@userAssesment');
    Route::post('user-assesment', 'API\UserController@countActivity');
    Route::get('thebest', 'API\UserController@theBest');
    Route::post('user-by-unit', 'API\UserController@userByUnit');
    Route::get('can-access', 'API\UserController@canAccessUser');
    Route::post('user-lapbul', 'API\UserController@userlapbul');

    Route::post('change-password', 'API\UserController@changePassword');
    Route::post('reset-password', 'API\UserController@resetPassword');

    //alamat
    Route::apiResources(['address' => 'API\AddressController']);

    //Menu Master Data Jabatan
    Route::get('jabatan-jfu', 'API\PositionController@showjfu');
    Route::get('jabatan-jft', 'API\PositionController@showjft');
    Route::get('jabatan', 'API\PositionController@showJab');
    Route::apiResources(['position' => 'API\PositionController']);

    //placement
    Route::apiResources(['placement' => 'API\PlacementController']);
    Route::post('get-atasan', 'API\PlacementController@getAtasan');

    //academic
    Route::apiResources(['academic' => 'API\AcademicController']);
    Route::post('academic/save', 'API\AcademicController@save');

    //address
    Route::apiResources(['address' => 'API\AddressController']);
    Route::get('get-address/{id}', 'API\AddressController@getAddressUser');

    //biodata
    Route::apiResources(['biodata' => 'API\BiodataController']);
    Route::get('get-biodata/{id}', 'API\BiodataController@getBiodataUser');

    //Menu Master Data Unit
    Route::apiResources(['unit' => 'API\UnitController']);
    Route::post('unitbyinstalasi', 'API\UnitController@unitByInstalasi');
    Route::post('unitbykomite', 'API\UnitController@unitByKomite');
    Route::post('unitbysubbagian', 'API\UnitController@unitBySubBagian');
    Route::post('unitbybagian', 'API\UnitController@unitByBagian');
    Route::post('unitbywadir', 'API\UnitController@unitByWadir');
    Route::get('selectunit', 'API\UnitController@selectUnit');
    Route::get('all-unit', 'API\UnitController@allUnit');
    Route::get('unit-by-placement', 'API\UnitController@unitByPlacement');

    //pegawai non aktif
    Route::apiResources(['disable-user' => 'API\DisableUserController']);

    //tupoksi
    Route::apiResources(['tupoksi' => 'API\TupoksiController']);
    Route::post('tupoksi-jabatan', 'API\TupoksiController@tupoksiJabatan');
    Route::get('tupoksibylogin', 'API\TupoksiController@tupoksiByLogin');

    //skp
    Route::apiResources(['skp' => 'API\SkpController']);
    Route::post('skp-pegawai', 'API\SkpController@skpPegawai');
    Route::post('get-skp-pegawai', 'API\SkpController@getSkpPegawai');
    Route::post('get-prestasi', 'API\SkpController@getPrestasi');

    //rkk
    Route::apiResources(['rkk' => 'API\RkkController']);

    //kegiatan pegawai
    Route::apiResources(['activity' => 'API\ActivityController']);
    Route::post('activity/by-date', 'API\ActivityController@byDate');
    Route::post('activity/get-event', 'API\ActivityController@getEvent');
    Route::post('activity/latest', 'API\ActivityController@latestActivity');

    //perilaku pegawai
    Route::apiResources(['behavior' => 'API\BehaviorController']);
    Route::post('user-behavior', 'API\BehaviorController@getByUser');

    // Menu Laporan Bulanan
    Route::apiResources(['report' => 'API\ReportController']);


    //dashboard
    Route::get('dashboard/count-Assesment', 'API\ActivityController@countAssesmentDB');
    Route::get('dashboard/birthday', 'API\BiodataController@Birthday');
    Route::post('dashboard/statistic', 'API\ActivityController@statisticDB');

});

